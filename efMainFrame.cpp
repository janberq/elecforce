#include "efMainFrame.h"
#include <wx/filedlg.h>
#include <wx/msgdlg.h>
#include <wx/aboutdlg.h>
#include <wx/filename.h>

efMainFrame::efMainFrame( wxWindow* parent )
:
ELECFORCE_GUI_MainFrame( parent )
{
	m_Simulation = new efSimulation;
	m_FilePath = wxT("");	// new file

	for (int i = 0; i < 10; i++)
		m_ColumnMap[i] = i;

	m_ColumnHeaders[0] = _("Name");
	m_ColumnHeaders[1] = _("X");
	m_ColumnHeaders[2] = _("Y");
	m_ColumnHeaders[3] = _("Electric Charge");
	m_ColumnHeaders[4] = _("Electric Force");
	m_ColumnHeaders[5] = _("Electric Force X");
	m_ColumnHeaders[6] = _("Electric Force Y");
	m_ColumnHeaders[7] = _("Electric Force Theta");
	m_ColumnHeaders[8] = _("Electric Potential");
	m_ColumnHeaders[9] = _("Radius");

	m_ObjectNameCounter = 0;

	DoRefreshStuff(true);
}

efMainFrame::~efMainFrame()
{
	if (m_Simulation != NULL)
		delete m_Simulation;
	m_Simulation = NULL;
}

void efMainFrame::DoListCtrlColumnStuff()
{
	for (int c = 0; c < 10; c++)
	{
		m_listCtrlObjects->InsertColumn(c, m_ColumnHeaders[m_ColumnMap[c]]);
	}
}

void efMainFrame::DoListCtrlInsertObject(size_t i, efObject *obj)
{
	m_listCtrlObjects->InsertItem(i, wxT(""));
	DoListCtrlEditObject(i, obj);
}

void efMainFrame::DoListCtrlEditObject(size_t i, efObject *obj)
{
	double electricForce, electricForceX, electricForceY, electricForceTheta;
	double electricField, electricFieldX, electricFieldY, electricFieldTheta;
	double electricPotential;

	electricForceX = m_Simulation->CalculateElectricForceX(obj);
	electricForceY = m_Simulation->CalculateElectricForceY(obj);
	electricForce = sqrt(electricForceX * electricForceX + electricForceY * electricForceY);
	electricForceTheta = atan2(electricForceY, electricForceX) * 180 / M_PI;
	electricPotential = m_Simulation->CalculateElectricPotential(obj);

	for (int j = 0; j < 10; j++)
	{
		switch (m_ColumnMap[j])
		{
		case 0:
			m_listCtrlObjects->SetItem(i, j, obj->Name);
			break;
		case 1:
			m_listCtrlObjects->SetItem(i, j, wxString::Format(wxT("%f"), obj->X));
			break;
		case 2:
			m_listCtrlObjects->SetItem(i, j, wxString::Format(wxT("%f"), obj->Y));
			break;
		case 3:
			m_listCtrlObjects->SetItem(i, j, wxString::Format(wxT("%f"), obj->ElectricCharge));
			break;
		case 4:
			m_listCtrlObjects->SetItem(i, j, wxString::Format(wxT("%f"), electricForce));
			break;
		case 5:
			m_listCtrlObjects->SetItem(i, j, wxString::Format(wxT("%f"), electricForceX));
			break;
		case 6:
			m_listCtrlObjects->SetItem(i, j, wxString::Format(wxT("%f"), electricForceY));
			break;
		case 7:
			m_listCtrlObjects->SetItem(i, j, wxString::Format(wxT("%f"), electricForceTheta));
			break;
		case 8:
			m_listCtrlObjects->SetItem(i, j, wxString::Format(wxT("%f"), electricPotential));
			break;
		case 9:
			m_listCtrlObjects->SetItem(i, j, wxString::Format(wxT("%f"), obj->Radius));
			break;
		default:
			break;
		}
	}
}

void efMainFrame::DoListCtrlSelectObject(size_t i)
{
	long item = -1;
	item = m_listCtrlObjects->GetNextItem(	item,
					wxLIST_NEXT_ALL,
					wxLIST_STATE_SELECTED);
	if ( item != -1 )	// first, unselect old
	{
		m_listCtrlObjects->SetItemState(item, 0, wxLIST_STATE_SELECTED|wxLIST_STATE_FOCUSED);
	}

	// then select it
	m_listCtrlObjects->SetItemState(i, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED);

}

void efMainFrame::DoListCtrlRefreshItems()
{
	int i = 0;
	for (	vector<efObject *>::iterator it = m_Simulation->Objects.begin();
		it != m_Simulation->Objects.end();
		it++)
	{
		DoListCtrlEditObject(i, *it);
		i++;
	}
}

void efMainFrame::DoRefreshSystemElectricPotential()
{
	m_textCtrlElecPot->SetValue(wxString::Format(wxT("%f"), m_Simulation->CalculateSystemElectricPotential()));
}

void efMainFrame::DoRefreshStuff(bool refreshControls)
{
	if (refreshControls)
	{
		// First refresh the title
		SetTitle((m_FilePath == wxT("") ? _("New Simulation") : m_FilePath) + wxT(" - Electric Force Simulation"));
		// Then, refresh the listctrl
		m_listCtrlObjects->ClearAll();
		DoListCtrlColumnStuff();
		int i = 0;
		for (	vector<efObject *>::iterator it = m_Simulation->Objects.begin();
			it != m_Simulation->Objects.end();
			it++)
		{
			DoListCtrlInsertObject(i, *it);
			i++;
		}
	}

	DoRefreshSystemElectricPotential();
	if (!refreshControls) DoListCtrlRefreshItems();
	m_DrawPanel->SetObjectListViewer(this);
	m_DrawPanel->SetSimulation(m_Simulation);
	m_DrawPanel->Refresh(true);
}

void efMainFrame::DoNewFileStuff()
{
	if (m_Simulation != NULL)
		delete m_Simulation;
	m_Simulation = new efSimulation;
	m_FilePath = wxT("");	// new file
	m_ObjectNameCounter = 0;
	DoRefreshStuff(true);
}

void efMainFrame::DoOpenFileStuff()
{
	wxFileDialog dlg(
		this,
		_("Choose a file to open"),
		wxT(""),
		wxT(""),
		wxT("JANBERQ Simulation Files (*.janberqsim)|*.janberqsim|XML Files (*.xml)|*.xml|All Files|*.*"),
		wxFD_OPEN | wxFD_FILE_MUST_EXIST);

	if (dlg.ShowModal() == wxID_OK)
	{
		if (m_Simulation != NULL)
			delete m_Simulation;
		m_Simulation = new efSimulation;
		m_FilePath = dlg.GetPath();	// new file
		m_Simulation->LoadFromFile(m_FilePath);
		m_ObjectNameCounter = m_Simulation->Objects.size();
		DoRefreshStuff(true);		// must also set title
	}
}

void efMainFrame::DoSaveFileStuff()
{
	if (m_FilePath == wxT(""))
	{
		wxFileDialog dlg(
			this,
			_("Choose a file to save"),
			wxT(""),
			wxT(""),
			wxT("JANBERQ Simulation Files (*.janberqsim)|*.janberqsim|XML Files (*.xml)|*.xml|All Files|*.*"),
			wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

		if (dlg.ShowModal() == wxID_OK)
		{
			m_FilePath = dlg.GetPath();
		}
	}

	if (m_FilePath != wxT(""))
	{
		m_Simulation->SaveToFile(m_FilePath);
	}
}

void efMainFrame::DoSaveFileAsStuff()
{
	wxFileDialog dlg(
		this,
		_("Choose a file to save"),
		wxT(""),
		wxT(""),
		wxT("JANBERQ Simulation Files (*.janberqsim)|*.janberqsim|Comma-seperated values (separator is ';') (*.csv)|*.csv|XML Files (*.xml)|*.xml|All Files|*.*"),
		wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

	if (dlg.ShowModal() == wxID_OK)
	{
		wxString ext;
		wxFileName::SplitPath(dlg.GetPath(), NULL, NULL, &ext);
		if (ext.Lower() == wxT("csv"))
		{
			m_Simulation->ExportToCSVFile(dlg.GetPath());
		}
		else
		{
			m_Simulation->SaveToFile(dlg.GetPath());
		}
	}
}

void efMainFrame::OnMainFrameClose( wxCloseEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsBeginDrag( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsBeginLabelEdit( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsBeginRDrag( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsCacheHint( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsColBeginDrag( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsColClick( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsColDragging( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsColEndDrag( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsColRightClick( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsDeleteAllItems( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsDeleteItem( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsEndLabelEdit( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsInsertItem( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsItemActivated( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsItemDeselected( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsItemFocused( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsItemMiddleClick( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsItemRightClick( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsItemSelected( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnListCtrlObjectsKeyDown( wxListEvent& event )
{
	event.Skip();
}

void efMainFrame::OnButtonAddClick( wxCommandEvent& event )
{
	m_Simulation->Objects.push_back(new efObject);
	(*(m_Simulation->Objects.rbegin()))->Name = wxString::Format(_("Unnamed Object %d"), m_ObjectNameCounter);
	(*(m_Simulation->Objects.rbegin()))->Simulation = m_Simulation;
	m_ObjectNameCounter++;
	DoListCtrlInsertObject(m_Simulation->Objects.size() - 1, *(m_Simulation->Objects.rbegin()));
	DoRefreshStuff(false);
}

void efMainFrame::OnButtonEditClick( wxCommandEvent& event )
{
	long item = -1;
	item = m_listCtrlObjects->GetNextItem(	item,
					wxLIST_NEXT_ALL,
					wxLIST_STATE_SELECTED);
	if ( item == -1 )
		return;

	wxDialog *dlg = m_Simulation->Objects[item]->CreateEditDialog(this);
	if (dlg->ShowModal() == wxID_OK)
	{
		DoListCtrlEditObject(item, m_Simulation->Objects[item]);
		DoRefreshStuff(false);
	}
	delete dlg;
}

void efMainFrame::OnButtonRemoveClick( wxCommandEvent& event )
{
	long item = -1;
	item = m_listCtrlObjects->GetNextItem(item,
			             wxLIST_NEXT_ALL,
			             wxLIST_STATE_SELECTED);
	if ( item == -1 )
		return;
	m_Simulation->Objects.erase(m_Simulation->Objects.begin() + item);
	m_listCtrlObjects->DeleteItem(item);
	DoRefreshStuff(false);
}

void efMainFrame::OnButtonMoveUpClick( wxCommandEvent& event )
{
	long item = -1;
	item = m_listCtrlObjects->GetNextItem(item,
			             wxLIST_NEXT_ALL,
			             wxLIST_STATE_SELECTED);
	if ( item <= 0 )
		return;

	efObject *old = m_Simulation->Objects[item - 1];
	m_Simulation->Objects[item - 1] = m_Simulation->Objects[item];
	m_Simulation->Objects[item] = old;
	DoListCtrlEditObject(item - 1, m_Simulation->Objects[item - 1]);
	DoListCtrlEditObject(item, m_Simulation->Objects[item]);

	m_listCtrlObjects->SetItemState(item, 0, wxLIST_STATE_SELECTED|wxLIST_STATE_FOCUSED);
	m_listCtrlObjects->SetItemState(item - 1, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED);
}

void efMainFrame::OnButtonMoveDownClick( wxCommandEvent& event )
{
	long item = -1;
	item = m_listCtrlObjects->GetNextItem(item,
			             wxLIST_NEXT_ALL,
			             wxLIST_STATE_SELECTED);
	if ( item == -1 )
	    return;

	if (item >= m_Simulation->Objects.size() - 1)
		return;

	efObject *old = m_Simulation->Objects[item + 1];
	m_Simulation->Objects[item + 1] = m_Simulation->Objects[item];
	m_Simulation->Objects[item] = old;
	DoListCtrlEditObject(item + 1, m_Simulation->Objects[item + 1]);
	DoListCtrlEditObject(item, m_Simulation->Objects[item]);

	m_listCtrlObjects->SetItemState(item, 0, wxLIST_STATE_SELECTED|wxLIST_STATE_FOCUSED);
	m_listCtrlObjects->SetItemState(item + 1, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED);
}

void efMainFrame::OnButtonClearClick( wxCommandEvent& event )
{
	m_Simulation->FreeMemory(false);
	DoRefreshStuff(true);
}

void efMainFrame::OnMenuItemNewSelection( wxCommandEvent& event )
{
	DoNewFileStuff();	// This is just a simple simulation, not a file editor.
				// So I don't have to ask user whether s/he wants to save the simulation
}

void efMainFrame::OnMenuItemOpenSelection( wxCommandEvent& event )
{
	DoOpenFileStuff();
}

void efMainFrame::OnMenuItemSaveSelection( wxCommandEvent& event )
{
	DoSaveFileStuff();
}

void efMainFrame::OnMenuItemSaveAsSelection( wxCommandEvent& event )
{
	DoSaveFileAsStuff();
}

void efMainFrame::OnMenuItemEditEnvironmentSelection( wxCommandEvent& event )
{
	if (m_Simulation != NULL)
	{
		wxDialog *dlg = m_Simulation->Environment->CreateEditDialog(this);
		if (dlg->ShowModal() == wxID_OK)
		{
			DoRefreshStuff(false);
		}
	}
}

void efMainFrame::OnMenuItemExitSelection( wxCommandEvent& event )
{
	Close();
}

void efMainFrame::OnMenuItemHelpSelection( wxCommandEvent& event )
{
	wxAboutDialogInfo info;
	info.SetName(wxT("Electric Force"));
	info.SetVersion(wxT("1.0 Alpha"));
	info.SetDescription(_("This application simulates electric force, field and some other electro static stuff. This application is written as a part of Canberk Sonmez's school project."));
	info.SetCopyright(wxT("(C) 2013 Canberk Sönmez <canberk.sonmez.409@gmail.com>"));
	wxAboutBox(info);
}
