/*
 * Canberk Sönmez
 *
 * efSimulation.cpp
 *
 * Saturday 17 Aug 2013
 *
 * Contains implementation of classes efSimulation and efObject
 *
 */

#include "efSimulation.h"
#include "efEditEnvironmentDialog.h"
#include "efEditObjectDialog.h"
#include "efEditEnvironmentDialog.h"

#include <wx/textfile.h>

#define DEFAULT_ELECTRIC_CHARGE 0.00005

/*
 * efObject Implementation
 */

efObject::efObject()
{
	LoadDefaults();
	Simulation = NULL;
}

void efObject::LoadDefaults()
{
	Name = wxT("Unnamed Object");
	ElectricCharge = DEFAULT_ELECTRIC_CHARGE;
	ElectricForce.Show = true;
	ElectricForce.Colour = wxColour(255, 0, 0);
	ElectricForceX.Show = false;
	ElectricForceX.Colour = wxColour(255, 166, 166);
	ElectricForceY.Show = false;
	ElectricForceY.Colour = wxColour(255, 166, 166);

	X = 0;
	Y = 0;
	Radius = 10;
	FillColour = wxColour(255, 255, 255);
	StrokeColour = wxColour(0, 0, 0);
}

wxXmlNode *efObject::SaveToXMLNode()
{
	wxXmlNode *node = new wxXmlNode(NULL, wxXML_ELEMENT_NODE, wxT("efObject"));

	wxXmlNode *child1, *child2, *child3;

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("Name"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, Name);

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("ElectricCharge"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, wxString::Format(wxT("%f"), ElectricCharge));

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("ElectricForce"));
	child2 = new wxXmlNode(child1, wxXML_ELEMENT_NODE, wxT("Show"));
	child3 = new wxXmlNode(child2, wxXML_TEXT_NODE, wxEmptyString, ElectricForce.Show ? wxT("true") : wxT("false"));
	child2 = new wxXmlNode(child1, wxXML_ELEMENT_NODE, wxT("Colour"));
	child3 = new wxXmlNode(child2, wxXML_TEXT_NODE, wxEmptyString, ElectricForce.Colour.GetAsString());

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("ElectricForceX"));
	child2 = new wxXmlNode(child1, wxXML_ELEMENT_NODE, wxT("Show"));
	child3 = new wxXmlNode(child2, wxXML_TEXT_NODE, wxEmptyString, ElectricForceX.Show ? wxT("true") : wxT("false"));
	child2 = new wxXmlNode(child1, wxXML_ELEMENT_NODE, wxT("Colour"));
	child3 = new wxXmlNode(child2, wxXML_TEXT_NODE, wxEmptyString, ElectricForceX.Colour.GetAsString());

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("ElectricForceY"));
	child2 = new wxXmlNode(child1, wxXML_ELEMENT_NODE, wxT("Show"));
	child3 = new wxXmlNode(child2, wxXML_TEXT_NODE, wxEmptyString, ElectricForceY.Show ? wxT("true") : wxT("false"));
	child2 = new wxXmlNode(child1, wxXML_ELEMENT_NODE, wxT("Colour"));
	child3 = new wxXmlNode(child2, wxXML_TEXT_NODE, wxEmptyString, ElectricForceY.Colour.GetAsString());

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("X"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, wxString::Format(wxT("%f"), X));

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("Y"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, wxString::Format(wxT("%f"), Y));

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("Radius"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, wxString::Format(wxT("%f"), Radius));

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("FillColour"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, FillColour.GetAsString());

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("StrokeColour"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, StrokeColour.GetAsString());

	return node;
}

bool efObject::LoadFromXMLNode(wxXmlNode *node)
{
	wxXmlNode *child1, *child2;
	child1 = node->GetChildren();
	while (child1)
	{
		if (child1->GetType() == wxXML_ELEMENT_NODE)
		{
			if (child1->GetName() == wxT("Name"))
			{
				Name = child1->GetNodeContent();
			}
			else if (child1->GetName() == wxT("ElectricCharge"))
			{
				if (! child1->GetNodeContent().ToDouble(&ElectricCharge))
				{ ElectricCharge = DEFAULT_ELECTRIC_CHARGE; }
			}
			else if (child1->GetName() == wxT("X"))
			{
				if (! child1->GetNodeContent().ToDouble(&X))
				{ X = 0; }
			}
			else if (child1->GetName() == wxT("Y"))
			{
				if (! child1->GetNodeContent().ToDouble(&Y))
				{ Y = 0; }
			}
			else if (child1->GetName() == wxT("Radius"))
			{
				if (! child1->GetNodeContent().ToDouble(&Radius))
				{ Radius = 0; }
			}
			else if (child1->GetName() == wxT("FillColour"))
			{
				if (! FillColour.Set(child1->GetNodeContent()))
				{ FillColour = wxColour(255, 255, 255); }
			}
			else if (child1->GetName() == wxT("StrokeColour"))
			{
				if (! StrokeColour.Set(child1->GetNodeContent()))
				{ StrokeColour = wxColour(0, 0, 0); }
			}
			else if (child1->GetName() == wxT("ElectricForce"))
			{
				child2 = child1->GetChildren();
				while (child2)
				{
					if (child2->GetType() == wxXML_ELEMENT_NODE)
					{
						if (child2->GetName() == wxT("Show"))
						{
							ElectricForce.Show = child2->GetNodeContent() != wxT("false");
						}
						else if (child2->GetName() == wxT("Colour"))
						{
							if (! ElectricForce.Colour.Set(child2->GetNodeContent()))
							{ ElectricForce.Colour = wxColour(255, 0, 0); }
						}
						else
						{ /* do nothing */ }
					}
					child2 = child2->GetNext();
				}
			}
			else if (child1->GetName() == wxT("ElectricForceX"))
			{
				child2 = child1->GetChildren();
				while (child2)
				{
					if (child2->GetType() == wxXML_ELEMENT_NODE)
					{
						if (child2->GetName() == wxT("Show"))
						{
							ElectricForceX.Show = child2->GetNodeContent() == wxT("true");
						}
						else if (child2->GetName() == wxT("Colour"))
						{
							if (! ElectricForceX.Colour.Set(child2->GetNodeContent()))
							{ ElectricForceX.Colour = wxColour(255, 166, 166); }
						}
						else
						{ /* do nothing */ }
					}
					child2 = child2->GetNext();
				}
			}
			else if (child1->GetName() == wxT("ElectricForceY"))
			{
				child2 = child1->GetChildren();
				while (child2)
				{
					if (child2->GetType() == wxXML_ELEMENT_NODE)
					{
						if (child2->GetName() == wxT("Show"))
						{
							ElectricForceY.Show = child2->GetNodeContent() == wxT("true");
						}
						else if (child2->GetName() == wxT("Colour"))
						{
							if (! ElectricForceY.Colour.Set(child2->GetNodeContent()))
							{ ElectricForceY.Colour = wxColour(255, 166, 166); }
						}
						else
						{ /* do nothing */ }
					}
					child2 = child2->GetNext();
				}
			}
			else
			{ /* do nothing */ }
		}

		child1 = child1->GetNext();
	}
}

wxDialog *efObject::CreateEditDialog(wxWindow *parent)
{
	return new efEditObjectDialog(parent, Simulation, this);
}

wxString efObject::CreateCSVStyleSummary(wxChar sep)
{
	double electricForce, electricForceX, electricForceY, electricForceTheta;
	double electricPotential;

	electricForceX = Simulation->CalculateElectricForceX(this);
	electricForceY = Simulation->CalculateElectricForceY(this);
	electricForce = sqrt(electricForceX * electricForceX + electricForceY * electricForceY);
	electricForceTheta = atan2(electricForceY, electricForceX) * 180 / M_PI;
	electricPotential = Simulation->CalculateElectricPotential(this);

	return wxString::Format(	wxString(wxT("%s")) + wxString(sep) +	/* Name */
					wxString(wxT("%f")) + wxString(sep) +	/* X */
					wxString(wxT("%f")) + wxString(sep) +	/* Y */
					wxString(wxT("%f")) + wxString(sep) +	/* Electric Charge */
					wxString(wxT("%f")) + wxString(sep) +	/* Electric Force */
					wxString(wxT("%f")) + wxString(sep) +	/* Electric Force X */
					wxString(wxT("%f")) + wxString(sep) +	/* Electric Force Y */
					wxString(wxT("%f")) + wxString(sep) +	/* Electric Force Theta*/
					wxString(wxT("%f")) + wxString(sep) +	/* Electric Potential */
					wxString(wxT("%f")) + wxString(sep)	/* Radius */
					,
					Name,
					X,
					Y,
					ElectricCharge,
					electricForce,
					electricForceX,
					electricForceY,
					electricForceTheta,
					electricPotential,
					Radius	);
}

efObject::~efObject()
{

}

/*
 * efSimulation::efEnvironment
 */

efSimulation::efEnvironment::efEnvironment()
{
	LoadDefaults();
}

void efSimulation::efEnvironment::LoadDefaults()
{
	k = 9e9;	// Really, I'm not sure.
	OX = 0;
	OY = 0;
	SX = 1;
	SY = 1;
	BackgroundColour = wxColour(255, 255, 255);
	GridColour = wxColour(0, 0, 0);
	ShowGrid = true;
	SnapToGrid = true;
	SpacePerMetre = 50;
}

wxXmlNode *efSimulation::efEnvironment::SaveToXMLNode()
{
	wxXmlNode *node = new wxXmlNode(NULL, wxXML_ELEMENT_NODE, wxT("efEnvironment"));

	wxXmlNode *child1, *child2;

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("k"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, wxString::Format(wxT("%f"), k));

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("OX"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, wxString::Format(wxT("%f"), OX));

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("OY"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, wxString::Format(wxT("%f"), OY));

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("SX"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, wxString::Format(wxT("%f"), SX));

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("SY"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, wxString::Format(wxT("%f"), SY));

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("SpacePerMetre"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, wxString::Format(wxT("%f"), SpacePerMetre));

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("BackgroundColour"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, BackgroundColour.GetAsString());

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("GridColour"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, GridColour.GetAsString());

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("ShowGrid"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, ShowGrid ? wxT("true") : wxT("false"));

	child1 = new wxXmlNode(node, wxXML_ELEMENT_NODE, wxT("SnapToGrid"));
	child2 = new wxXmlNode(child1, wxXML_TEXT_NODE, wxEmptyString, SnapToGrid ? wxT("true") : wxT("false"));

	return node;
}

bool efSimulation::efEnvironment::LoadFromXMLNode(wxXmlNode *node)
{
	wxXmlNode *child1;
	child1 = node->GetChildren();
	while (child1)
	{
		if (child1->GetType() == wxXML_ELEMENT_NODE)
		{
			if (child1->GetName() == wxT("k"))
			{
				if (! child1->GetNodeContent().ToDouble(&k))
				{ k = 9e9; }
			}
			else if (child1->GetName() == wxT("OX"))
			{
				if (! child1->GetNodeContent().ToDouble(&OX))
				{ OX = 0; }
			}
			else if (child1->GetName() == wxT("OY"))
			{
				if (! child1->GetNodeContent().ToDouble(&OY))
				{ OY = 0; }
			}
			else if (child1->GetName() == wxT("SX"))
			{
				if (! child1->GetNodeContent().ToDouble(&SX))
				{ SX = 1; }
			}
			else if (child1->GetName() == wxT("SY"))
			{
				if (! child1->GetNodeContent().ToDouble(&SY))
				{ SY = 1; }
			}
			else if (child1->GetName() == wxT("SpacePerMetre"))
			{
				if (! child1->GetNodeContent().ToDouble(&SpacePerMetre))
				{ SpacePerMetre = 50; }
			}
			else if (child1->GetName() == wxT("BackgroundColour"))
			{
				if (! BackgroundColour.Set(child1->GetNodeContent()))
				{ BackgroundColour = wxColour(255, 255, 255); }
			}
			else if (child1->GetName() == wxT("GridColour"))
			{
				if (! GridColour.Set(child1->GetNodeContent()))
				{ GridColour = wxColour(0, 0, 0); }
			}
			else if (child1->GetName() == wxT("ShowGrid"))
			{
				ShowGrid = child1->GetNodeContent() != wxT("false");
			}
			else if (child1->GetName() == wxT("SnapToGrid"))
			{
				ShowGrid = child1->GetNodeContent() != wxT("false");
			}
			else
			{ /* do nothing */ }
		}

		child1 = child1->GetNext();
	}
}

wxDialog *efSimulation::efEnvironment::CreateEditDialog(wxWindow *parent)
{
	return new efEditEnvironmentDialog(parent, this);
}

efSimulation::efEnvironment::~efEnvironment()
{

}

/*
 * efSimulation Implementation
 */

bool efSimulation::LoadFromFile(const wxString &file)
{
	wxXmlDocument doc;
	if (!doc.Load(file))
		return false;

	wxXmlNode *root = doc.GetRoot();

	if (root == NULL)
		return false;

	if (root->GetType() != wxXML_ELEMENT_NODE)
		return false;

	if (root->GetName() != wxT("efSimulation"))
		return false;

	wxXmlNode *child1 = root->GetChildren();

	while (child1)
	{

		if (child1->GetName() == wxT("efEnvironment"))
		{
			if (Environment == NULL)
				Environment = new efEnvironment;
			Environment->LoadDefaults();
			Environment->LoadFromXMLNode(child1);
		}
		else if (child1->GetName() == wxT("efObject"))
		{
			efObject *obj = new efObject;
			obj->LoadFromXMLNode(child1);
			obj->Simulation = this;
			Objects.push_back(obj);
		}
		else
		{ /* do nothing */}

		child1 = child1->GetNext();
	}

	return true;
}

bool efSimulation::SaveToFile(const wxString &file) 
{
	wxXmlDocument doc;
	wxXmlNode *root = new wxXmlNode(NULL, wxXML_ELEMENT_NODE, wxT("efSimulation"));
	doc.SetRoot(root);

	wxXmlNode *child;

	root->AddChild(child = (Environment->SaveToXMLNode()));

	for (vector<efObject *>::iterator it = Objects.begin();
		it != Objects.end();
		it ++)
	{
		wxXmlNode *temp;
		child->SetNext(temp = ((*it)->SaveToXMLNode()));
		child = temp;
	}

	doc.Save(file);

	return true;
}

bool efSimulation::ExportToCSVFile(const wxString &file)
{
	wxTextFile tf(file);
	for (vector<efObject *>::iterator it = Objects.begin();
		it != Objects.end();
		it ++)
	{
		tf.AddLine((*it)->CreateCSVStyleSummary());
	}
	tf.Write();
	return true;
}

bool efSimulation::FreeMemory(bool remEnviron)
{
	if (Environment != NULL && remEnviron)
	{
		delete Environment;
		Environment = NULL;
	}
	for (vector<efObject *>::iterator it = Objects.begin();
		it != Objects.end();
		it ++)
	{
		delete *it;
	}
	Objects.clear();
	return true;
}

wxPoint efSimulation::TransformPoint(double x, double y)
{
	if (Environment == NULL)
		return wxPoint(-1, -1);
	return wxPoint(	Environment->OX + Environment->SX * x * Environment->SpacePerMetre,
			Environment->OY + Environment->SY * y * Environment->SpacePerMetre);
}

double efSimulation::CalculateElectricForceX(const efObject *obj) const
{
	double ret = 0, alpha;
	double dsqr;
	for (vector<efObject *>::const_iterator it = Objects.begin();
		it != Objects.end();
		it++)
	{
		dsqr = (obj->X - (*it)->X) * (obj->X - (*it)->X) + (obj->Y - (*it)->Y) * (obj->Y - (*it)->Y);
		if (dsqr != 0)
		{
			alpha = atan2(obj->Y - (*it)->Y, obj->X - (*it)->X);
			ret += (Environment->k * (*it)->ElectricCharge * obj->ElectricCharge / dsqr) * cos(alpha);
		}
	}
	return ret;
}

double efSimulation::CalculateElectricForceY(const efObject *obj) const
{
	double ret = 0, alpha;
	double dsqr;
	for (vector<efObject *>::const_iterator it = Objects.begin();
		it != Objects.end();
		it++)
	{
		dsqr = (obj->X - (*it)->X) * (obj->X - (*it)->X) + (obj->Y - (*it)->Y) * (obj->Y - (*it)->Y);
		if (dsqr != 0)
		{
			alpha = atan2(obj->Y - (*it)->Y, obj->X - (*it)->X);
			ret += (Environment->k * (*it)->ElectricCharge * obj->ElectricCharge / dsqr) * sin(alpha);
		}
	}
	return ret;
}

double efSimulation::CalculateElectricPotential(const efObject *obj) const
{
	double ret = 0;
	double d;
	for (vector<efObject *>::const_iterator it = Objects.begin();
		it != Objects.end();
		it++)
	{
		d = sqrt((obj->X - (*it)->X) * (obj->X - (*it)->X) + (obj->Y - (*it)->Y) * (obj->Y - (*it)->Y));
		if (d != 0)
		{
			ret += (Environment->k * (*it)->ElectricCharge * obj->ElectricCharge / d);
		}
	}
	return ret;
}

double efSimulation::CalculateSystemElectricPotential() const
{
	double ret = 0, d;
	for (size_t i = 0; i < Objects.size(); i++)
		for (size_t j = i + 1; j < Objects.size(); j++)
	{
		d = sqrt((Objects[i]->X - Objects[j]->X) * (Objects[i]->X - Objects[j]->X) + (Objects[i]->Y - Objects[j]->Y) * (Objects[i]->Y - Objects[j]->Y));
		if (d != 0)
		{
			ret += (Environment->k * Objects[i]->ElectricCharge * Objects[j]->ElectricCharge) / d;
		}
	}
	return ret;
}

efSimulation::efSimulation()
{
	Environment = new efEnvironment;
}

efSimulation::~efSimulation()
{
	FreeMemory();
}

