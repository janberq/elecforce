��    B      ,  Y   <      �     �     �     �     �     �     �     �  /   �     �     �                    3     I     O     h     q     v     �     �  *   �     �     �     �          &     7     H  "   [     ~     �  "   �     �     �  	   �     �     �     �     �  
   �  
     �        �     �     �     �     �  C   
	     N	     f	     w	  	   �	     �	     �	     �	  �   �	     ]
     o
     �
     �
     �
     �
     �
     �
    �
     �     �     �     �     �     �  	   �  L   �          !     (     7     ?  !   ^     �     �     �     �     �     �     �  ,        /     ?     P     n     �     �     �  !   �     �     �          "     /     1     A     P     V     g  
   i  
   t  �     	   Q     [     q  	   u       \   �     �          4     S     e     t     �  �   �     1  "   C     f     h     j     m     p     s     =      >           
      '           /       B                        :                        3              (          7   &      9   6   ,   $         8   %          4          +   .       #       	   "             5   @          ?   *                 )             A   1      2   ;         !      -       <      0       &File &Help &New &Open &Quit &Save About About the wonderful author of this application. Add Apply Background Colour Cancel Choose a file to open Choose a file to save Clear Create a new simulation. Defaults Edit Edit Environment Edit Environment Settings Edit Object Edit environmental constants and settings. Electric Charge Electric Force Electric Force Simulation Electric Force Theta Electric Force X Electric Force Y Electric Potential Electrical Potential (j)
of system Exit from application. Fill Colour Get objects from an existing file. Grid Colour H Move Down Move Up Name New Simulation O Origin (x) Origin (y) Please type the space you want to use for the force between two particles with 1C-charge and 1 m distance.
Then, click button "Apply" which will make Scale factors 1 and calculates "space per meter". Radius Radius (in px) Remove Reset Save As/Export Save file in a different format, such as CSV. CSV cannot be loaded. Save objects to a file. Scale Factor (x) Scale Factor (y) Show Grid Snap to grid Space per metre Stroke Colour This application simulates electric force, field and some other electro static stuff. This application is written as a part of Canberk Sonmez's school project. Unnamed Object %d Use this option carefully ! X Y Z+ Z- Z0 k Project-Id-Version: ELECFORCE
POT-Creation-Date: 2013-08-21 15:13+0200
PO-Revision-Date: 2013-08-21 15:27+0200
Last-Translator: Canberk Sönmez <canberk.sonmez.409@gmail.com>
Language-Team: Janberq <canberk.sonmez.409@gmail.com>
Language: Turkish
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: /home/canberk/Desktop
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ELECFORCE
 &Dosya &Yardım &Yeni &Aç &Çık Kaydet Hakkında Canberk Sönmez tarafından yazılmış olan bu uygulamayla ilgili bilgiler. Ekle Uygula Arkaplan Rengi Vazgeç Açmak İçin Bir Dosya Seçin Kaydetmek İçin Bir Dosya Seçin Temizle Yeni bir simülasyon oluştur. Varsayılanlar Düzenle Çevreyi düzenle Çevresel Seçenekleri Düzenle Nesneyi Düzenle Çevre sabitlerini ve ayarlarını düzenle. Elektrik Yükü Elektrik Kuvveti Elektrik Kuvveti Simülasyonu Elektrik Kuvveti Açısı Elektrik Kuvveti X Elektrik Kuvveti Y Elektrik Potansiyeli Sistemin Elektrik
Potansiyeli (j) Uygulamadan çık. Doldurma Rengi Bir dosyadan nesneleri al. Izgara Rengi H Aşağı Taşı Yukarı Taşı İsim Yeni Simülasyon O Orijin (x) Orijin (y) Buraya 1 metre uzaklıktaki 1C'luk yükler arasındaki kuvvetin kaç px ile gösterileceğini yazın.
Sonra "Uygula"ya basın ki "Boyutlandırma Faktör"leri "1" olsun ve "Metre başına boşluk" hesaplansın. Yarıçap Yarıçap (px olarak) Sil Sıfırla Farklı Kaydet/Dışa Aktar Dosyayı farklı bir formatta kaydet, örneğin CSV. CSV formatı sonradan geri yüklenemez. Bir dosyaya nesneleri aktar. Boyutlandırma Katsayısı (x) Boyutlandırma Katsayısı (y) Izgarayı Göster Izgaraya Yasla Metre başına boşluk Çizgi Rengi Bu uygulama elektrik kuvveti gibi çeşitli elektro statik kavramları simüle eder. Bu uygulama Canberk Sönmez'in okul projesi olarak yazılmıştır. İsimsiz Nesne %d Bu seçeneği dikkatli kullanın ! X Y Z+ Z- Z0 k 