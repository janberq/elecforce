/*
 * Canberk Sönmez
 *
 * efSimulation.h
 *
 * Saturday 17 Aug 2013
 *
 * I started this project about 3 days ago
 *
 * Contains definition of classes efSimulation and efObject
 *
 */

#ifndef _efSimulation_H_
#define _efSimulation_H_

#include <wx/string.h>
#include <wx/xml/xml.h>
#include <wx/colour.h>
#include <wx/dialog.h>
#include <cmath>
#include <vector>
using std::vector;

class efObject;
class efSimulation;

class efObject
{

public:

	efObject();

	efSimulation *Simulation;	// for CreateEditDialog() and CreateCSVStyleSummary()

	struct {
		bool Show;
		wxColour Colour;
	}
		ElectricForce,
		ElectricForceX,
		ElectricForceY;
	wxString Name;
	double ElectricCharge;
	double X;
	double Y;
	double Radius;
	wxColour FillColour;
	wxColour StrokeColour;
	virtual void LoadDefaults();
	virtual wxXmlNode *SaveToXMLNode();
	virtual bool LoadFromXMLNode(wxXmlNode *node);
	virtual wxDialog *CreateEditDialog(wxWindow *parent);
	virtual wxString CreateCSVStyleSummary(wxChar sep = wxT(';'));
	virtual ~efObject();
};

class efSimulation
{

public:

	struct efEnvironment
	{
		efEnvironment();
		double k;
		double OX;
		double OY;
		double SX;
		double SY;
		wxColour BackgroundColour;
		wxColour GridColour;
		bool ShowGrid;
		bool SnapToGrid;
		double SpacePerMetre;
		virtual void LoadDefaults();
		virtual wxXmlNode *SaveToXMLNode();
		virtual bool LoadFromXMLNode(wxXmlNode *node);
		virtual wxDialog *CreateEditDialog(wxWindow *parent);
		virtual ~efEnvironment();
	} *Environment;

	vector<efObject *> Objects;

	virtual bool LoadFromFile(const wxString &file);
	virtual bool SaveToFile(const wxString &file);
	virtual bool ExportToCSVFile(const wxString &file);
	virtual bool FreeMemory(bool remEnviron = true);

	virtual wxPoint TransformPoint(double x, double y);

	virtual double CalculateElectricForceX(const efObject *obj) const;
	virtual double CalculateElectricForceY(const efObject *obj) const;
	virtual double CalculateElectricPotential(const efObject *obj) const;
	virtual double CalculateSystemElectricPotential() const;

	efSimulation();
	virtual ~efSimulation();

};

#endif // _efSimulation_H_
