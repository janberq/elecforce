#include "efEditEnvironmentDialog.h"

efEditEnvironmentDialog::efEditEnvironmentDialog( wxWindow* parent, efSimEnvironment *obj  )
:
ELECFORCE_GUI_EditEnvironmentDialog( parent )
{
	m_ObjectToEdit = obj;
	TransferDataToWindow();
}

bool efEditEnvironmentDialog::TransferDataFromWindow()

{
	if (m_ObjectToEdit == NULL)
		return false;

	m_textCtrlConstantK->GetValue().ToDouble(&(m_ObjectToEdit->k));
	m_textCtrlOriginX->GetValue().ToDouble(&(m_ObjectToEdit->OX));
	m_textCtrlOriginY->GetValue().ToDouble(&(m_ObjectToEdit->OY));
	m_textCtrlScaleFactorX->GetValue().ToDouble(&(m_ObjectToEdit->SX));
	m_textCtrlScaleFactorY->GetValue().ToDouble(&(m_ObjectToEdit->SY));
	m_textCtrlSpacePerMetre->GetValue().ToDouble(&(m_ObjectToEdit->SpacePerMetre));
	m_ObjectToEdit->BackgroundColour = m_colourPickerBackgroundColour->GetColour();
	m_ObjectToEdit->GridColour = m_colourPickerGridColour->GetColour();
	m_ObjectToEdit->ShowGrid = m_checkBoxShowGrid->GetValue();
	m_ObjectToEdit->SnapToGrid = m_checkBoxSnapToGrid->GetValue();

	return true;
}

bool efEditEnvironmentDialog::TransferDataToWindow()
{
	if (m_ObjectToEdit == NULL)
		return false;

	m_textCtrlConstantK->SetValue(wxString::Format(wxT("%f"), m_ObjectToEdit->k));
	m_textCtrlOriginX->SetValue(wxString::Format(wxT("%f"), m_ObjectToEdit->OX));
	m_textCtrlOriginY->SetValue(wxString::Format(wxT("%f"), m_ObjectToEdit->OY));
	m_textCtrlScaleFactorX->SetValue(wxString::Format(wxT("%f"), m_ObjectToEdit->SX));
	m_textCtrlScaleFactorY->SetValue(wxString::Format(wxT("%f"), m_ObjectToEdit->SY));
	m_textCtrlSpacePerMetre->SetValue(wxString::Format(wxT("%f"), m_ObjectToEdit->SpacePerMetre));
	m_colourPickerBackgroundColour->SetColour(m_ObjectToEdit->BackgroundColour);
	m_colourPickerGridColour->SetColour(m_ObjectToEdit->GridColour);
	m_checkBoxShowGrid->SetValue(m_ObjectToEdit->ShowGrid);
	m_checkBoxSnapToGrid->SetValue(m_ObjectToEdit->SnapToGrid);

	return true;
}

void efEditEnvironmentDialog::OnButtonApplyMySpecialClick( wxCommandEvent& event )
{

	if (TransferDataFromWindow())
	{
		double special;
		if (m_textCtrlMySpecialValue->GetValue().ToDouble(&special))
		{
			m_ObjectToEdit->SX = 1;
		m_ObjectToEdit->SY = 1;
			m_ObjectToEdit->SpacePerMetre = special / m_ObjectToEdit->k;
		}

		TransferDataToWindow();
	}

}

void efEditEnvironmentDialog::OnButtonApplyClick( wxCommandEvent& event )
{
	if (Validate() && TransferDataFromWindow())
	{
		if (IsModal())
		{
			EndModal(wxID_OK);
		}
		else
		{
			SetReturnCode(wxID_OK);
			this->Show(false);
		}
	}
	else
	{
		if (IsModal())
		{
			EndModal(wxID_CANCEL);
		}
		else
		{
			SetReturnCode(wxID_CANCEL);
			this->Show(false);
		}
	}
}

void efEditEnvironmentDialog::OnButtonCancelClick( wxCommandEvent& event )
{
	if (IsModal())
	{
		EndModal(wxID_CANCEL);
	}
	else
	{
		SetReturnCode(wxID_CANCEL);
		this->Show(false);
	}
}

void efEditEnvironmentDialog::OnButtonResetClick( wxCommandEvent& event )
{
	TransferDataToWindow();
}

void efEditEnvironmentDialog::OnButtonDefaultsClick( wxCommandEvent& event )
{
	efSimEnvironment *oldobj = m_ObjectToEdit;
	m_ObjectToEdit = new efSimEnvironment;
	TransferDataToWindow();
	delete m_ObjectToEdit;
	m_ObjectToEdit = oldobj;
}
