#include <wx/app.h>
#include "efMainFrame.h"

#if 0

#include "efEditObjectDialog.h"
#include "efEditEnvironmentDialog.h"
#include "efSimulation.h"

#endif

class MyApp : public wxApp
{
public:
	virtual bool OnInit()
	{
#if 0
		efMainFrame *mainfrm = new efMainFrame(NULL);
		efEditObjectDialog *editobjdlg = new efEditObjectDialog(NULL);
		efEditEnvironmentDialog *editenvdlg = new efEditEnvironmentDialog(NULL);
		mainfrm->Show(true);
		editobjdlg->Show(true);
		editenvdlg->Show(true);
		return true;
#endif

#if 0
		efObject x;
		wxXmlDocument doc;
		doc.SetRoot(x.SaveToXMLNode());
		doc.Save(wxT("test.xml"));
#endif

#if 0
		efObject y;
		wxXmlDocument docr;
		docr.Load(wxT("./test.xml"));
		y.LoadFromXMLNode(docr.GetRoot());
		wxXmlDocument docw;
		docw.SetRoot(y.SaveToXMLNode());
		docw.Save(wxT("test2.xml"));
#endif

		efMainFrame *mainfrm = new efMainFrame(NULL);
		mainfrm->Show(true);
		return true;

	}
};

DECLARE_APP(MyApp)

IMPLEMENT_APP(MyApp)

