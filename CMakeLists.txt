cmake_minimum_required(VERSION 3.0)

# A project that I wrote soo long time ago:
# Sal 16 Tem 2013 18∶47∶14 EEST

project(ELECFORCE)

# set module path
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/CMakeModules ${CMAKE_MODULE_PATH})

# set the executable name
set(EXE ${PROJECT_NAME})

# set sources
set(
    EXE_SRCS
	efApp.cpp
	efDrawPanel.cpp
	efEditEnvironmentDialog.cpp
	efEditObjectDialog.cpp
	efMainFrame.cpp
	efSimulation.cpp
	ELECFORCE.cpp
)

IF(WIN32)
  set(EXE_SRCS ${EXE_SRCS} resources.rc)
ENDIF(WIN32)

# find the required packages
# find_package(Lua 5.3 REQUIRED)
# find_package(Sol2 REQUIRED)
find_package(wxWidgets REQUIRED COMPONENTS core base xml adv)
# find_package(Boost REQUIRED COMPONENTS locale filesystem system)
# find_package(Eigen3 REQUIRED)

# message(STATUS "${LUA_VERSION_STRING}")
# message(STATUS "${LUA_LIBRARIES}")

add_executable(${EXE} ${EXE_SRCS})

# configure the executable

set( CMAKE_CXX_EXTENSIONS ON)
set_target_properties(
    ${EXE}
    PROPERTIES
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

# set(
#     SOL2_SECURITY
#     SOL_SAFE_USERTYPE
#     SOL_CHECK_ARGUMENTS
# )

target_compile_definitions(
    ${EXE}
    PUBLIC ${wxWidgets_DEFINITIONS}
#    PUBLIC ${SOL2_SECURITY}
#    PUBLIC ${PRSS_DEFINITIONS}
)

target_compile_options(
    ${EXE}
    PUBLIC ${wxWidgets_CXX_FLAGS}
    PUBLIC ${ADDITIONAL_COMPILE_OPTIONS}
)

target_link_libraries(
    ${EXE}
    PUBLIC ${wxWidgets_LIBRARIES}
#     PUBLIC ${LUA_LIBRARIES}
    PUBLIC ${Boost_LIBRARIES}
)

target_include_directories(
    ${EXE}
    PUBLIC ${CMAKE_SOURCE_DIR}
    PUBLIC ${wxWidgets_INCLUDE_DIRS}
#     PUBLIC ${LUA_INCLUDE_DIR}
#     PUBLIC ${SOL2_INCLUDE_DIR}
#     PUBLIC ${BOOST_INCLUDEDIR}
#     PUBLIC ${EIGEN3_INCLUDE_DIR}
)

install(TARGETS ${EXE} DESTINATION bin)

