/*
 * 20/08/2013 11:32
 * écriré par Canberk Sönmez
 * pour son projét d'école.
 * cette fichier est un conteneur pour
 * le classe 
 * c'est assez FRANÇAIS !
 */

#ifndef _efDrawPanel_H_
#define _efDrawPanel_H_

#include "efSimulation.h"
#include <wx/panel.h>
#include <wx/button.h>

extern const wxString efDrawPanelName;

class efObjectListViewer;
class efDrawPanel;

class efObjectListViewer	// for some important stuff
{
protected:

	friend class efDrawPanel;

	virtual void DoListCtrlInsertObject(size_t i, efObject *obj) = 0;
	virtual void DoListCtrlEditObject(size_t i, efObject *obj) = 0;
	virtual void DoListCtrlSelectObject(size_t i) = 0;
	virtual void DoListCtrlRefreshItems() = 0;
	virtual void DoRefreshSystemElectricPotential() = 0;

};

class efDrawPanel : public wxPanel
{

protected:

	// Member Variables
	efSimulation *m_Simulation;
	efObjectListViewer *m_ObjectListViewer;
	bool m_AreButtonsHidden;
	bool m_DrawAxes;
	efObject *m_MovingObject;
	size_t m_MovingObjectIndex;
	wxPoint m_PointA;
	bool m_IsLeftDown;

	struct grid_line
	{
		double value;
		int pos;
		grid_line(const double &_value, const double &_pos)
		{ value = _value; pos = _pos; }
	};

	vector<grid_line> m_GridLinesX, m_GridLinesY;

	// Grid Functions
	virtual void CalculateGrid();
	
	// Drawing Functions
	virtual void DrawArrow(wxDC &dc, double x1, double y1, double x2, double y2);
	virtual void DrawGrid(wxDC &dc);
	virtual void DrawAxisNumbers(wxDC &dc);	// not much important
	virtual void DrawObject(wxDC &dc, const efObject *obj);
	virtual void DrawInformation(wxDC &dc);
	virtual wxPoint TransformPoint(const wxPoint &p);
	virtual efObject *FindObjectFromPoint(const wxPoint &p, size_t &index);
	
	// Event Handlers for wxPanel
	virtual void OnPaint(wxPaintEvent &event);
	virtual void OnEraseBackground(wxEraseEvent &event);
	virtual void OnMouseLeftDown(wxMouseEvent &event);
	virtual void OnMouseLeftUp(wxMouseEvent &event);
	virtual void OnMouseLeftDclick(wxMouseEvent &event);
	virtual void OnMouseRightDown(wxMouseEvent &event);
	virtual void OnMouseRightUp(wxMouseEvent &event);
	virtual void OnMouseRightDclick(wxMouseEvent &event);
	virtual void OnMouseMiddleDown(wxMouseEvent &event);
	virtual void OnMouseMiddleUp(wxMouseEvent &event);
	virtual void OnMouseMiddleDclick(wxMouseEvent &event);
	virtual void OnMouseMotion(wxMouseEvent &event);
	virtual void OnMouseWheel(wxMouseEvent &event);
	virtual void OnKeyDown(wxKeyEvent &event);
	virtual void OnKeyUp(wxKeyEvent &event);

	// IDs for wxButton's
	enum
	{
		ID_ButtonZoomIn  = 1000,
		ID_ButtonZoomOut,
		ID_ButtonZoomReset,
		ID_ButtonResetOrigin,
		ID_ButtonHideButtons
	};
	
	// Event Handlers for wxButton's
	virtual void OnButtonZoomInClick(wxCommandEvent &event);
	virtual void OnButtonZoomOutClick(wxCommandEvent &event);
	virtual void OnButtonZoomResetClick(wxCommandEvent &event);
	virtual void OnButtonResetOriginClick(wxCommandEvent &event);
	virtual void OnButtonHideButtonsClick(wxCommandEvent &event);
	
	// Control ptrs
	wxButton *m_ButtonZoomIn;
	wxButton *m_ButtonZoomOut;
	wxButton *m_ButtonZoomReset;
	wxButton *m_ButtonResetOrigin;
	wxButton *m_ButtonHideButtons;
	
public:

	virtual void SetSimulation(efSimulation *sim, bool refresh = true);
	virtual efSimulation *GetSimulation() const;

	virtual void SetObjectListViewer(efObjectListViewer *view);
	virtual efObjectListViewer *GetObjectListViewer() const;

	virtual void ZoomIn();
	virtual void ZoomOut();
	virtual void ZoomReset();
	virtual void ResetOrigin();

	efDrawPanel();
	efDrawPanel(
		wxWindow *parent,
		wxWindowID id = wxID_ANY,
		const wxPoint &pos = wxDefaultPosition,
		const wxSize &size = wxDefaultSize,
		long style = wxTAB_TRAVERSAL,
		const wxString &name = efDrawPanelName
	);
	virtual bool Create(
		wxWindow *parent,
		wxWindowID id = wxID_ANY,
		const wxPoint &pos = wxDefaultPosition,
		const wxSize &size = wxDefaultSize,
		long style = wxTAB_TRAVERSAL,
		const wxString &name = efDrawPanelName
	);
	virtual ~efDrawPanel();

private:

	DECLARE_EVENT_TABLE()

};

#endif // _efDrawPanel_H_
