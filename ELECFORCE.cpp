///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct  8 2012)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "ELECFORCE.h"

///////////////////////////////////////////////////////////////////////////

ELECFORCE_GUI_MainFrame::ELECFORCE_GUI_MainFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizerMain;
	bSizerMain = new wxBoxSizer( wxVERTICAL );
	
	m_splitterMain = new wxSplitterWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D|wxTAB_TRAVERSAL );
	m_splitterMain->SetSashGravity( 0.9 );
	m_splitterMain->SetSashSize( 0 );
	m_splitterMain->Connect( wxEVT_IDLE, wxIdleEventHandler( ELECFORCE_GUI_MainFrame::m_splitterMainOnIdle ), NULL, this );
	m_splitterMain->SetMinimumPaneSize( 10 );
	
	m_panel1 = new wxPanel( m_splitterMain, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerPanel1Main;
	bSizerPanel1Main = new wxBoxSizer( wxVERTICAL );
	
	m_DrawPanel = new efDrawPanel(m_panel1);
	bSizerPanel1Main->Add( m_DrawPanel, 1, wxALL|wxEXPAND, 5 );
	
	
	m_panel1->SetSizer( bSizerPanel1Main );
	m_panel1->Layout();
	bSizerPanel1Main->Fit( m_panel1 );
	m_panel2 = new wxPanel( m_splitterMain, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerPanel2Main;
	bSizerPanel2Main = new wxBoxSizer( wxVERTICAL );
	
	m_listCtrlObjects = new wxListCtrl( m_panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_EDIT_LABELS|wxLC_REPORT|wxTAB_TRAVERSAL );
	bSizerPanel2Main->Add( m_listCtrlObjects, 1, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizerItemActions;
	bSizerItemActions = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizerActions1;
	bSizerActions1 = new wxBoxSizer( wxHORIZONTAL );
	
	m_bpButtonAdd = new wxButton( m_panel2, wxID_ANY, _("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerActions1->Add( m_bpButtonAdd, 1, wxALL, 5 );
	
	m_bpButtonEdit = new wxButton( m_panel2, wxID_ANY, _("Edit"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerActions1->Add( m_bpButtonEdit, 1, wxALL, 5 );
	
	m_bpButtonRemove = new wxButton( m_panel2, wxID_ANY, _("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerActions1->Add( m_bpButtonRemove, 1, wxALL, 5 );
	
	
	bSizerItemActions->Add( bSizerActions1, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizerActions2;
	bSizerActions2 = new wxBoxSizer( wxHORIZONTAL );
	
	m_bpButtonMoveUp = new wxButton( m_panel2, wxID_ANY, _("Move Up"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerActions2->Add( m_bpButtonMoveUp, 1, wxALL, 5 );
	
	m_bpButtonMoveDown = new wxButton( m_panel2, wxID_ANY, _("Move Down"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerActions2->Add( m_bpButtonMoveDown, 1, wxALL, 5 );
	
	m_bpButtonClear = new wxButton( m_panel2, wxID_ANY, _("Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerActions2->Add( m_bpButtonClear, 1, wxALL, 5 );
	
	
	bSizerItemActions->Add( bSizerActions2, 1, wxEXPAND, 5 );
	
	
	bSizerPanel2Main->Add( bSizerItemActions, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizerElecPot;
	bSizerElecPot = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText1 = new wxStaticText( m_panel2, wxID_ANY, _("Electrical Potential (j)\nof system"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	bSizerElecPot->Add( m_staticText1, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrlElecPot = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizerElecPot->Add( m_textCtrlElecPot, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerPanel2Main->Add( bSizerElecPot, 0, wxEXPAND, 5 );
	
	
	m_panel2->SetSizer( bSizerPanel2Main );
	m_panel2->Layout();
	bSizerPanel2Main->Fit( m_panel2 );
	m_splitterMain->SplitVertically( m_panel1, m_panel2, 0 );
	bSizerMain->Add( m_splitterMain, 1, wxEXPAND, 0 );
	
	
	this->SetSizer( bSizerMain );
	this->Layout();
	m_menubarMain = new wxMenuBar( 0 );
	m_menuFile = new wxMenu();
	wxMenuItem* m_menuItemNew;
	m_menuItemNew = new wxMenuItem( m_menuFile, wxID_NEW, wxString( _("&New") ) + wxT('\t') + wxT("CTRL+E"), _("Create a new simulation."), wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItemNew->SetBitmaps( wxArtProvider::GetBitmap( wxART_NEW, wxART_MENU ) );
	#elif defined( __WXGTK__ )
	m_menuItemNew->SetBitmap( wxArtProvider::GetBitmap( wxART_NEW, wxART_MENU ) );
	#endif
	m_menuFile->Append( m_menuItemNew );
	
	wxMenuItem* m_menuItemOpen;
	m_menuItemOpen = new wxMenuItem( m_menuFile, wxID_OPEN, wxString( _("&Open") ) + wxT('\t') + wxT("CTRL+O"), _("Get objects from an existing file."), wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItemOpen->SetBitmaps( wxArtProvider::GetBitmap( wxART_FILE_OPEN, wxART_MENU ) );
	#elif defined( __WXGTK__ )
	m_menuItemOpen->SetBitmap( wxArtProvider::GetBitmap( wxART_FILE_OPEN, wxART_MENU ) );
	#endif
	m_menuFile->Append( m_menuItemOpen );
	
	wxMenuItem* m_menuItemSave;
	m_menuItemSave = new wxMenuItem( m_menuFile, wxID_SAVE, wxString( _("&Save") ) + wxT('\t') + wxT("CTRL+S"), _("Save objects to a file."), wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItemSave->SetBitmaps( wxArtProvider::GetBitmap( wxART_FILE_SAVE, wxART_MENU ) );
	#elif defined( __WXGTK__ )
	m_menuItemSave->SetBitmap( wxArtProvider::GetBitmap( wxART_FILE_SAVE, wxART_MENU ) );
	#endif
	m_menuFile->Append( m_menuItemSave );
	
	wxMenuItem* m_menuItemSaveAs;
	m_menuItemSaveAs = new wxMenuItem( m_menuFile, wxID_SAVEAS, wxString( _("Save As/Export") ) + wxT('\t') + wxT("CTRL+SHIFT+S"), _("Save file in a different format, such as CSV. CSV cannot be loaded."), wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItemSaveAs->SetBitmaps( wxArtProvider::GetBitmap( wxART_FILE_SAVE_AS, wxART_MENU ) );
	#elif defined( __WXGTK__ )
	m_menuItemSaveAs->SetBitmap( wxArtProvider::GetBitmap( wxART_FILE_SAVE_AS, wxART_MENU ) );
	#endif
	m_menuFile->Append( m_menuItemSaveAs );
	
	m_menuFile->AppendSeparator();
	
	wxMenuItem* m_menuItemEditEnvironment;
	m_menuItemEditEnvironment = new wxMenuItem( m_menuFile, ID_EDITENVIRONMENT, wxString( _("Edit Environment") ) + wxT('\t') + wxT("CTRL+E"), _("Edit environmental constants and settings."), wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItemEditEnvironment->SetBitmaps( wxArtProvider::GetBitmap( wxART_REPORT_VIEW, wxART_MENU ) );
	#elif defined( __WXGTK__ )
	m_menuItemEditEnvironment->SetBitmap( wxArtProvider::GetBitmap( wxART_REPORT_VIEW, wxART_MENU ) );
	#endif
	m_menuFile->Append( m_menuItemEditEnvironment );
	
	m_menuFile->AppendSeparator();
	
	wxMenuItem* m_menuItemExit;
	m_menuItemExit = new wxMenuItem( m_menuFile, wxID_EXIT, wxString( _("&Quit") ) + wxT('\t') + wxT("ALT+F4"), _("Exit from application."), wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItemExit->SetBitmaps( wxArtProvider::GetBitmap( wxART_QUIT, wxART_MENU ) );
	#elif defined( __WXGTK__ )
	m_menuItemExit->SetBitmap( wxArtProvider::GetBitmap( wxART_QUIT, wxART_MENU ) );
	#endif
	m_menuFile->Append( m_menuItemExit );
	
	m_menubarMain->Append( m_menuFile, _("&File") ); 
	
	m_menuHelp = new wxMenu();
	wxMenuItem* m_menuItemHelp;
	m_menuItemHelp = new wxMenuItem( m_menuHelp, wxID_ABOUT, wxString( _("About") ) + wxT('\t') + wxT("F1"), _("About the wonderful author of this application."), wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItemHelp->SetBitmaps( wxArtProvider::GetBitmap( wxART_INFORMATION, wxART_MENU ) );
	#elif defined( __WXGTK__ )
	m_menuItemHelp->SetBitmap( wxArtProvider::GetBitmap( wxART_INFORMATION, wxART_MENU ) );
	#endif
	m_menuHelp->Append( m_menuItemHelp );
	
	m_menubarMain->Append( m_menuHelp, _("&Help") ); 
	
	this->SetMenuBar( m_menubarMain );
	
	m_statusBarMain = this->CreateStatusBar( 1, wxST_SIZEGRIP, wxID_ANY );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( ELECFORCE_GUI_MainFrame::OnMainFrameClose ) );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_BEGIN_DRAG, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsBeginDrag ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_BEGIN_LABEL_EDIT, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsBeginLabelEdit ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_BEGIN_RDRAG, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsBeginRDrag ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_CACHE_HINT, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsCacheHint ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_COL_BEGIN_DRAG, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsColBeginDrag ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_COL_CLICK, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsColClick ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_COL_DRAGGING, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsColDragging ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_COL_END_DRAG, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsColEndDrag ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_COL_RIGHT_CLICK, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsColRightClick ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_DELETE_ALL_ITEMS, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsDeleteAllItems ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_DELETE_ITEM, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsDeleteItem ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_END_LABEL_EDIT, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsEndLabelEdit ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_INSERT_ITEM, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsInsertItem ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_ITEM_ACTIVATED, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemActivated ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_ITEM_DESELECTED, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemDeselected ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_ITEM_FOCUSED, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemFocused ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_ITEM_MIDDLE_CLICK, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemMiddleClick ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_ITEM_RIGHT_CLICK, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemRightClick ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_ITEM_SELECTED, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemSelected ), NULL, this );
	m_listCtrlObjects->Connect( wxEVT_COMMAND_LIST_KEY_DOWN, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsKeyDown ), NULL, this );
	m_bpButtonAdd->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonAddClick ), NULL, this );
	m_bpButtonEdit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonEditClick ), NULL, this );
	m_bpButtonRemove->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonRemoveClick ), NULL, this );
	m_bpButtonMoveUp->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonMoveUpClick ), NULL, this );
	m_bpButtonMoveDown->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonMoveDownClick ), NULL, this );
	m_bpButtonClear->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonClearClick ), NULL, this );
	this->Connect( m_menuItemNew->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemNewSelection ) );
	this->Connect( m_menuItemOpen->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemOpenSelection ) );
	this->Connect( m_menuItemSave->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemSaveSelection ) );
	this->Connect( m_menuItemSaveAs->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemSaveAsSelection ) );
	this->Connect( m_menuItemEditEnvironment->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemEditEnvironmentSelection ) );
	this->Connect( m_menuItemExit->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemExitSelection ) );
	this->Connect( m_menuItemHelp->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemHelpSelection ) );
}

ELECFORCE_GUI_MainFrame::~ELECFORCE_GUI_MainFrame()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( ELECFORCE_GUI_MainFrame::OnMainFrameClose ) );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_BEGIN_DRAG, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsBeginDrag ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_BEGIN_LABEL_EDIT, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsBeginLabelEdit ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_BEGIN_RDRAG, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsBeginRDrag ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_CACHE_HINT, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsCacheHint ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_COL_BEGIN_DRAG, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsColBeginDrag ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_COL_CLICK, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsColClick ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_COL_DRAGGING, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsColDragging ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_COL_END_DRAG, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsColEndDrag ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_COL_RIGHT_CLICK, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsColRightClick ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_DELETE_ALL_ITEMS, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsDeleteAllItems ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_DELETE_ITEM, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsDeleteItem ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_END_LABEL_EDIT, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsEndLabelEdit ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_INSERT_ITEM, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsInsertItem ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_ITEM_ACTIVATED, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemActivated ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_ITEM_DESELECTED, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemDeselected ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_ITEM_FOCUSED, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemFocused ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_ITEM_MIDDLE_CLICK, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemMiddleClick ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_ITEM_RIGHT_CLICK, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemRightClick ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_ITEM_SELECTED, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsItemSelected ), NULL, this );
	m_listCtrlObjects->Disconnect( wxEVT_COMMAND_LIST_KEY_DOWN, wxListEventHandler( ELECFORCE_GUI_MainFrame::OnListCtrlObjectsKeyDown ), NULL, this );
	m_bpButtonAdd->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonAddClick ), NULL, this );
	m_bpButtonEdit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonEditClick ), NULL, this );
	m_bpButtonRemove->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonRemoveClick ), NULL, this );
	m_bpButtonMoveUp->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonMoveUpClick ), NULL, this );
	m_bpButtonMoveDown->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonMoveDownClick ), NULL, this );
	m_bpButtonClear->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnButtonClearClick ), NULL, this );
	this->Disconnect( wxID_NEW, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemNewSelection ) );
	this->Disconnect( wxID_OPEN, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemOpenSelection ) );
	this->Disconnect( wxID_SAVE, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemSaveSelection ) );
	this->Disconnect( wxID_SAVEAS, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemSaveAsSelection ) );
	this->Disconnect( ID_EDITENVIRONMENT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemEditEnvironmentSelection ) );
	this->Disconnect( wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemExitSelection ) );
	this->Disconnect( wxID_ABOUT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( ELECFORCE_GUI_MainFrame::OnMenuItemHelpSelection ) );
	
}

ELECFORCE_GUI_EditEnvironmentDialog::ELECFORCE_GUI_EditEnvironmentDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizerBase;
	bSizerBase = new wxBoxSizer( wxVERTICAL );
	
	bSizerBase->SetMinSize( wxSize( 600,400 ) ); 
	m_scrolledWindowBase = new wxScrolledWindow( this, wxID_ANY, wxDefaultPosition, wxSize( 400,400 ), wxHSCROLL|wxTAB_TRAVERSAL|wxVSCROLL );
	m_scrolledWindowBase->SetScrollRate( 5, 5 );
	wxBoxSizer* bSizerMainBase;
	bSizerMainBase = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizerMain;
	bSizerMain = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText1 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("k"), wxDefaultPosition, wxSize( 150,-1 ), wxALIGN_LEFT );
	m_staticText1->Wrap( -1 );
	bSizer1->Add( m_staticText1, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_textCtrlConstantK = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1->Add( m_textCtrlConstantK, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer1, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText9 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Origin (x)"), wxDefaultPosition, wxSize( 150,-1 ), wxALIGN_LEFT );
	m_staticText9->Wrap( -1 );
	bSizer9->Add( m_staticText9, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_textCtrlOriginX = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( m_textCtrlOriginX, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer9, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText10 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Origin (y)"), wxDefaultPosition, wxSize( 150,-1 ), wxALIGN_LEFT );
	m_staticText10->Wrap( -1 );
	bSizer10->Add( m_staticText10, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_textCtrlOriginY = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer10->Add( m_textCtrlOriginY, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer10, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText2 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Scale Factor (x)"), wxDefaultPosition, wxSize( 150,-1 ), wxALIGN_LEFT );
	m_staticText2->Wrap( -1 );
	bSizer2->Add( m_staticText2, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_textCtrlScaleFactorX = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer2->Add( m_textCtrlScaleFactorX, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer2, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText3 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Scale Factor (y)"), wxDefaultPosition, wxSize( 150,-1 ), wxALIGN_LEFT );
	m_staticText3->Wrap( -1 );
	bSizer3->Add( m_staticText3, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_textCtrlScaleFactorY = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer3->Add( m_textCtrlScaleFactorY, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer3, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText5 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Background Colour"), wxDefaultPosition, wxSize( 150,-1 ), wxALIGN_LEFT );
	m_staticText5->Wrap( -1 );
	bSizer5->Add( m_staticText5, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_colourPickerBackgroundColour = new wxColourPickerCtrl( m_scrolledWindowBase, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer5->Add( m_colourPickerBackgroundColour, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer5, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText6 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Grid Colour"), wxDefaultPosition, wxSize( 150,-1 ), wxALIGN_LEFT );
	m_staticText6->Wrap( -1 );
	bSizer6->Add( m_staticText6, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_colourPickerGridColour = new wxColourPickerCtrl( m_scrolledWindowBase, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer6->Add( m_colourPickerGridColour, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer6, 0, wxEXPAND, 5 );
	
	m_checkBoxShowGrid = new wxCheckBox( m_scrolledWindowBase, wxID_ANY, _("Show Grid"), wxDefaultPosition, wxDefaultSize, 0 );
	m_checkBoxShowGrid->SetValue(true); 
	bSizerMain->Add( m_checkBoxShowGrid, 0, wxALL|wxEXPAND, 5 );
	
	m_checkBoxSnapToGrid = new wxCheckBox( m_scrolledWindowBase, wxID_ANY, _("Snap to grid"), wxDefaultPosition, wxDefaultSize, 0 );
	m_checkBoxSnapToGrid->SetValue(true); 
	bSizerMain->Add( m_checkBoxSnapToGrid, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText7 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Space per metre"), wxDefaultPosition, wxSize( 150,-1 ), wxALIGN_LEFT );
	m_staticText7->Wrap( -1 );
	bSizer7->Add( m_staticText7, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_textCtrlSpacePerMetre = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_textCtrlSpacePerMetre, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer7, 0, wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizer1;
	sbSizer1 = new wxStaticBoxSizer( new wxStaticBox( m_scrolledWindowBase, wxID_ANY, _("Use this option carefully !") ), wxVERTICAL );
	
	m_staticText8 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Please type the space you want to use for the force between two particles with 1C-charge and 1 m distance.\nThen, click button \"Apply\" which will make Scale factors 1 and calculates \"space per meter\"."), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT|wxST_NO_AUTORESIZE );
	m_staticText8->Wrap( -1 );
	m_staticText8->SetMinSize( wxSize( 300,100 ) );
	
	sbSizer1->Add( m_staticText8, 0, wxALL|wxEXPAND, 5 );
	
	m_textCtrlMySpecialValue = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	sbSizer1->Add( m_textCtrlMySpecialValue, 0, wxALL|wxEXPAND, 5 );
	
	m_buttonApplyMySpecial = new wxButton( m_scrolledWindowBase, ID_APPLYMYSPECIAL, _("Apply"), wxDefaultPosition, wxDefaultSize, 0 );
	sbSizer1->Add( m_buttonApplyMySpecial, 0, wxALL|wxEXPAND, 5 );
	
	
	bSizerMain->Add( sbSizer1, 0, wxALL|wxEXPAND, 5 );
	
	
	bSizerMainBase->Add( bSizerMain, 1, wxALL|wxEXPAND, 10 );
	
	
	m_scrolledWindowBase->SetSizer( bSizerMainBase );
	m_scrolledWindowBase->Layout();
	bSizerBase->Add( m_scrolledWindowBase, 1, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizerButtons;
	bSizerButtons = new wxBoxSizer( wxHORIZONTAL );
	
	m_buttonApply = new wxButton( this, wxID_OK, _("Apply"), wxDefaultPosition, wxDefaultSize, 0 );
	m_buttonApply->SetDefault();
	bSizerButtons->Add( m_buttonApply, 0, wxALL, 5 );
	
	m_buttonCancel = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerButtons->Add( m_buttonCancel, 0, wxALL, 5 );
	
	m_buttonReset = new wxButton( this, wxID_RESET, _("Reset"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerButtons->Add( m_buttonReset, 0, wxALL, 5 );
	
	m_buttonDefaults = new wxButton( this, wxID_DEFAULT, _("Defaults"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerButtons->Add( m_buttonDefaults, 0, wxALL, 5 );
	
	
	bSizerBase->Add( bSizerButtons, 0, wxEXPAND, 5 );
	
	
	this->SetSizer( bSizerBase );
	this->Layout();
	bSizerBase->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_buttonApplyMySpecial->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditEnvironmentDialog::OnButtonApplyMySpecialClick ), NULL, this );
	m_buttonApply->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditEnvironmentDialog::OnButtonApplyClick ), NULL, this );
	m_buttonCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditEnvironmentDialog::OnButtonCancelClick ), NULL, this );
	m_buttonReset->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditEnvironmentDialog::OnButtonResetClick ), NULL, this );
	m_buttonDefaults->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditEnvironmentDialog::OnButtonDefaultsClick ), NULL, this );
}

ELECFORCE_GUI_EditEnvironmentDialog::~ELECFORCE_GUI_EditEnvironmentDialog()
{
	// Disconnect Events
	m_buttonApplyMySpecial->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditEnvironmentDialog::OnButtonApplyMySpecialClick ), NULL, this );
	m_buttonApply->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditEnvironmentDialog::OnButtonApplyClick ), NULL, this );
	m_buttonCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditEnvironmentDialog::OnButtonCancelClick ), NULL, this );
	m_buttonReset->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditEnvironmentDialog::OnButtonResetClick ), NULL, this );
	m_buttonDefaults->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditEnvironmentDialog::OnButtonDefaultsClick ), NULL, this );
	
}

ELECFORCE_GUI_EditObjectDialog::ELECFORCE_GUI_EditObjectDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizerBase;
	bSizerBase = new wxBoxSizer( wxVERTICAL );
	
	bSizerBase->SetMinSize( wxSize( 600,400 ) ); 
	m_scrolledWindowBase = new wxScrolledWindow( this, wxID_ANY, wxDefaultPosition, wxSize( 400,400 ), wxHSCROLL|wxTAB_TRAVERSAL|wxVSCROLL );
	m_scrolledWindowBase->SetScrollRate( 5, 5 );
	wxBoxSizer* bSizerMainBase;
	bSizerMainBase = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizerMain;
	bSizerMain = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText14 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Name"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	m_staticText14->Wrap( -1 );
	bSizer14->Add( m_staticText14, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrlName = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer14->Add( m_textCtrlName, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer14, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText16 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("X"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	m_staticText16->Wrap( -1 );
	bSizer16->Add( m_staticText16, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrlX = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer16->Add( m_textCtrlX, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer16, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer17;
	bSizer17 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText17 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Y"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	m_staticText17->Wrap( -1 );
	bSizer17->Add( m_staticText17, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrlY = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer17->Add( m_textCtrlY, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer17, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText9 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Electric Charge"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	m_staticText9->Wrap( -1 );
	bSizer9->Add( m_staticText9, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrlElectricCharge = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( m_textCtrlElectricCharge, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer9, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxHORIZONTAL );
	
	m_checkBoxShowElectricForce = new wxCheckBox( m_scrolledWindowBase, wxID_ANY, _("Electric Force"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	m_checkBoxShowElectricForce->SetValue(true); 
	bSizer1->Add( m_checkBoxShowElectricForce, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrlElectricForce = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer1->Add( m_textCtrlElectricForce, 1, wxALL, 5 );
	
	m_colourPickerElectricForce = new wxColourPickerCtrl( m_scrolledWindowBase, wxID_ANY, wxColour( 255, 0, 0 ), wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer1->Add( m_colourPickerElectricForce, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerMain->Add( bSizer1, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer15;
	bSizer15 = new wxBoxSizer( wxHORIZONTAL );
	
	m_checkBoxShowElectricForceX = new wxCheckBox( m_scrolledWindowBase, wxID_ANY, _("Electric Force X"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	bSizer15->Add( m_checkBoxShowElectricForceX, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrlElectricForceX = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer15->Add( m_textCtrlElectricForceX, 1, wxALL, 5 );
	
	m_colourPickerElectricForceX = new wxColourPickerCtrl( m_scrolledWindowBase, wxID_ANY, wxColour( 255, 166, 166 ), wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer15->Add( m_colourPickerElectricForceX, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerMain->Add( bSizer15, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxHORIZONTAL );
	
	m_checkBoxShowElectricForceY = new wxCheckBox( m_scrolledWindowBase, wxID_ANY, _("Electric Force Y"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	bSizer2->Add( m_checkBoxShowElectricForceY, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrlElectricForceY = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer2->Add( m_textCtrlElectricForceY, 1, wxALL, 5 );
	
	m_colourPickerElectricForceY = new wxColourPickerCtrl( m_scrolledWindowBase, wxID_ANY, wxColour( 255, 166, 166 ), wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer2->Add( m_colourPickerElectricForceY, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerMain->Add( bSizer2, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText3 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Electric Force Theta"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	m_staticText3->Wrap( -1 );
	bSizer3->Add( m_staticText3, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrlElectricForceTheta = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer3->Add( m_textCtrlElectricForceTheta, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer3, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText8 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Electric Potential"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	m_staticText8->Wrap( -1 );
	bSizer8->Add( m_staticText8, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrlElectricPotential = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer8->Add( m_textCtrlElectricPotential, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer8, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText10 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Radius (in px)"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	m_staticText10->Wrap( -1 );
	bSizer10->Add( m_staticText10, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrlRadius = new wxTextCtrl( m_scrolledWindowBase, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer10->Add( m_textCtrlRadius, 1, wxALL, 5 );
	
	
	bSizerMain->Add( bSizer10, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText12 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Fill Colour"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	m_staticText12->Wrap( -1 );
	bSizer12->Add( m_staticText12, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_colourPickerFillColour = new wxColourPickerCtrl( m_scrolledWindowBase, wxID_ANY, wxColour( 255, 255, 255 ), wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer12->Add( m_colourPickerFillColour, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizer11->Add( bSizer12, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText13 = new wxStaticText( m_scrolledWindowBase, wxID_ANY, _("Stroke Colour"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	m_staticText13->Wrap( -1 );
	bSizer13->Add( m_staticText13, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_colourPickerStrokeColour = new wxColourPickerCtrl( m_scrolledWindowBase, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer13->Add( m_colourPickerStrokeColour, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizer11->Add( bSizer13, 0, wxEXPAND, 5 );
	
	
	bSizerMain->Add( bSizer11, 0, wxEXPAND, 5 );
	
	
	bSizerMainBase->Add( bSizerMain, 1, wxALL|wxEXPAND, 10 );
	
	
	m_scrolledWindowBase->SetSizer( bSizerMainBase );
	m_scrolledWindowBase->Layout();
	bSizerBase->Add( m_scrolledWindowBase, 1, wxEXPAND | wxALL, 5 );
	
	wxBoxSizer* bSizerButtons;
	bSizerButtons = new wxBoxSizer( wxHORIZONTAL );
	
	m_buttonApply = new wxButton( this, wxID_OK, _("Apply"), wxDefaultPosition, wxDefaultSize, 0 );
	m_buttonApply->SetDefault();
	bSizerButtons->Add( m_buttonApply, 0, wxALL, 5 );
	
	m_buttonCancel = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 ); 
	bSizerButtons->Add( m_buttonCancel, 0, wxALL, 5 );
	
	m_buttonReset = new wxButton( this, wxID_RESET, _("Reset"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerButtons->Add( m_buttonReset, 0, wxALL, 5 );
	
	m_buttonDefaults = new wxButton( this, wxID_DEFAULT, _("Defaults"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerButtons->Add( m_buttonDefaults, 0, wxALL, 5 );
	
	
	bSizerBase->Add( bSizerButtons, 0, wxEXPAND, 5 );
	
	
	this->SetSizer( bSizerBase );
	this->Layout();
	bSizerBase->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_buttonApply->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditObjectDialog::OnButtonApplyClick ), NULL, this );
	m_buttonCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditObjectDialog::OnButtonCancelClick ), NULL, this );
	m_buttonReset->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditObjectDialog::OnButtonResetClick ), NULL, this );
	m_buttonDefaults->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditObjectDialog::OnButtonDefaultsClick ), NULL, this );
}

ELECFORCE_GUI_EditObjectDialog::~ELECFORCE_GUI_EditObjectDialog()
{
	// Disconnect Events
	m_buttonApply->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditObjectDialog::OnButtonApplyClick ), NULL, this );
	m_buttonCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditObjectDialog::OnButtonCancelClick ), NULL, this );
	m_buttonReset->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditObjectDialog::OnButtonResetClick ), NULL, this );
	m_buttonDefaults->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ELECFORCE_GUI_EditObjectDialog::OnButtonDefaultsClick ), NULL, this );
	
}
