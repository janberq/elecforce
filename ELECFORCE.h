///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct  8 2012)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __ELECFORCE_H__
#define __ELECFORCE_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/listctrl.h>
#include <wx/button.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/splitter.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/clrpicker.h>
#include <wx/checkbox.h>
#include <wx/statbox.h>
#include <wx/scrolwin.h>
#include <wx/dialog.h>

#include "efDrawPanel.h"

///////////////////////////////////////////////////////////////////////////

#define ID_EDITENVIRONMENT 1000
#define ID_APPLYMYSPECIAL 1001

///////////////////////////////////////////////////////////////////////////////
/// Class ELECFORCE_GUI_MainFrame
///////////////////////////////////////////////////////////////////////////////
class ELECFORCE_GUI_MainFrame : public wxFrame 
{
	private:
	
	protected:
		wxSplitterWindow* m_splitterMain;
		wxPanel* m_panel1;
		efDrawPanel *m_DrawPanel;
		wxPanel* m_panel2;
		wxListCtrl* m_listCtrlObjects;
		wxButton* m_bpButtonAdd;
		wxButton* m_bpButtonEdit;
		wxButton* m_bpButtonRemove;
		wxButton* m_bpButtonMoveUp;
		wxButton* m_bpButtonMoveDown;
		wxButton* m_bpButtonClear;
		wxStaticText* m_staticText1;
		wxTextCtrl* m_textCtrlElecPot;
		wxMenuBar* m_menubarMain;
		wxMenu* m_menuFile;
		wxMenu* m_menuHelp;
		wxStatusBar* m_statusBarMain;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnMainFrameClose( wxCloseEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsBeginDrag( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsBeginLabelEdit( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsBeginRDrag( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsCacheHint( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsColBeginDrag( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsColClick( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsColDragging( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsColEndDrag( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsColRightClick( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsDeleteAllItems( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsDeleteItem( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsEndLabelEdit( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsInsertItem( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsItemActivated( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsItemDeselected( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsItemFocused( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsItemMiddleClick( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsItemRightClick( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsItemSelected( wxListEvent& event ) { event.Skip(); }
		virtual void OnListCtrlObjectsKeyDown( wxListEvent& event ) { event.Skip(); }
		virtual void OnButtonAddClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonEditClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonRemoveClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonMoveUpClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonMoveDownClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonClearClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuItemNewSelection( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuItemOpenSelection( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuItemSaveSelection( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuItemSaveAsSelection( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuItemEditEnvironmentSelection( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuItemExitSelection( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuItemHelpSelection( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		ELECFORCE_GUI_MainFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Electric Force Simulation"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 800,300 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		
		~ELECFORCE_GUI_MainFrame();
		
		void m_splitterMainOnIdle( wxIdleEvent& )
		{
			m_splitterMain->SetSashPosition( 0 );
			m_splitterMain->Disconnect( wxEVT_IDLE, wxIdleEventHandler( ELECFORCE_GUI_MainFrame::m_splitterMainOnIdle ), NULL, this );
		}
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class ELECFORCE_GUI_EditEnvironmentDialog
///////////////////////////////////////////////////////////////////////////////
class ELECFORCE_GUI_EditEnvironmentDialog : public wxDialog 
{
	private:
	
	protected:
		wxScrolledWindow* m_scrolledWindowBase;
		wxStaticText* m_staticText1;
		wxTextCtrl* m_textCtrlConstantK;
		wxStaticText* m_staticText9;
		wxTextCtrl* m_textCtrlOriginX;
		wxStaticText* m_staticText10;
		wxTextCtrl* m_textCtrlOriginY;
		wxStaticText* m_staticText2;
		wxTextCtrl* m_textCtrlScaleFactorX;
		wxStaticText* m_staticText3;
		wxTextCtrl* m_textCtrlScaleFactorY;
		wxStaticText* m_staticText5;
		wxColourPickerCtrl* m_colourPickerBackgroundColour;
		wxStaticText* m_staticText6;
		wxColourPickerCtrl* m_colourPickerGridColour;
		wxCheckBox* m_checkBoxShowGrid;
		wxCheckBox* m_checkBoxSnapToGrid;
		wxStaticText* m_staticText7;
		wxTextCtrl* m_textCtrlSpacePerMetre;
		wxStaticText* m_staticText8;
		wxTextCtrl* m_textCtrlMySpecialValue;
		wxButton* m_buttonApplyMySpecial;
		wxButton* m_buttonApply;
		wxButton* m_buttonCancel;
		wxButton* m_buttonReset;
		wxButton* m_buttonDefaults;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnButtonApplyMySpecialClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonApplyClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonCancelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonResetClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonDefaultsClick( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		ELECFORCE_GUI_EditEnvironmentDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Edit Environment Settings"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxCAPTION|wxCLOSE_BOX|wxMAXIMIZE_BOX|wxMINIMIZE_BOX|wxRESIZE_BORDER|wxSYSTEM_MENU ); 
		~ELECFORCE_GUI_EditEnvironmentDialog();
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class ELECFORCE_GUI_EditObjectDialog
///////////////////////////////////////////////////////////////////////////////
class ELECFORCE_GUI_EditObjectDialog : public wxDialog 
{
	private:
	
	protected:
		wxScrolledWindow* m_scrolledWindowBase;
		wxStaticText* m_staticText14;
		wxTextCtrl* m_textCtrlName;
		wxStaticText* m_staticText16;
		wxTextCtrl* m_textCtrlX;
		wxStaticText* m_staticText17;
		wxTextCtrl* m_textCtrlY;
		wxStaticText* m_staticText9;
		wxTextCtrl* m_textCtrlElectricCharge;
		wxCheckBox* m_checkBoxShowElectricForce;
		wxTextCtrl* m_textCtrlElectricForce;
		wxColourPickerCtrl* m_colourPickerElectricForce;
		wxCheckBox* m_checkBoxShowElectricForceX;
		wxTextCtrl* m_textCtrlElectricForceX;
		wxColourPickerCtrl* m_colourPickerElectricForceX;
		wxCheckBox* m_checkBoxShowElectricForceY;
		wxTextCtrl* m_textCtrlElectricForceY;
		wxColourPickerCtrl* m_colourPickerElectricForceY;
		wxStaticText* m_staticText3;
		wxTextCtrl* m_textCtrlElectricForceTheta;
		wxStaticText* m_staticText8;
		wxTextCtrl* m_textCtrlElectricPotential;
		wxStaticText* m_staticText10;
		wxTextCtrl* m_textCtrlRadius;
		wxStaticText* m_staticText12;
		wxColourPickerCtrl* m_colourPickerFillColour;
		wxStaticText* m_staticText13;
		wxColourPickerCtrl* m_colourPickerStrokeColour;
		wxButton* m_buttonApply;
		wxButton* m_buttonCancel;
		wxButton* m_buttonReset;
		wxButton* m_buttonDefaults;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnButtonApplyClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonCancelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonResetClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonDefaultsClick( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		ELECFORCE_GUI_EditObjectDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Edit Object"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxCAPTION|wxCLOSE_BOX|wxMAXIMIZE_BOX|wxMINIMIZE_BOX|wxRESIZE_BORDER|wxSYSTEM_MENU ); 
		~ELECFORCE_GUI_EditObjectDialog();
	
};

#endif //__ELECFORCE_H__
