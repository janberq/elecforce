/*
 * 20/08/2013 13:10
 * C'est l'implementation du fichier
 * efDrawPanel.h  . Regardez-lui pour
 * puis information. (Excusez-moi
 * pour le mouvais français.)
 *
 * Ecriré par Canberk Sönmez,
 * Pour son projét d'école.
 *
 */

#include "efDrawPanel.h"
#include <wx/dcbuffer.h>
#include <cmath>

const wxString efDrawPanelName = wxT("efDrawPanel");

// Register event handlers
BEGIN_EVENT_TABLE(efDrawPanel, wxPanel)
	EVT_PAINT			(efDrawPanel::OnPaint)
	EVT_ERASE_BACKGROUND		(efDrawPanel::OnEraseBackground)
	EVT_LEFT_DOWN			(efDrawPanel::OnMouseLeftDown)
	EVT_LEFT_UP			(efDrawPanel::OnMouseLeftUp)
	EVT_LEFT_DCLICK			(efDrawPanel::OnMouseLeftDclick)
	EVT_RIGHT_DOWN			(efDrawPanel::OnMouseRightDown)
	EVT_RIGHT_UP			(efDrawPanel::OnMouseRightUp)
	EVT_RIGHT_DCLICK		(efDrawPanel::OnMouseRightDclick)
	EVT_MIDDLE_DOWN			(efDrawPanel::OnMouseMiddleDown)
	EVT_MIDDLE_UP			(efDrawPanel::OnMouseMiddleUp)
	EVT_MIDDLE_DCLICK		(efDrawPanel::OnMouseMiddleDclick)
	EVT_MOTION			(efDrawPanel::OnMouseMotion)
	EVT_MOUSEWHEEL			(efDrawPanel::OnMouseWheel)
	EVT_KEY_DOWN			(efDrawPanel::OnKeyDown)
	EVT_KEY_UP			(efDrawPanel::OnKeyUp)
	EVT_BUTTON			(efDrawPanel::ID_ButtonZoomIn,
	efDrawPanel::OnButtonZoomInClick)
	EVT_BUTTON			(efDrawPanel::ID_ButtonZoomOut,
	efDrawPanel::OnButtonZoomOutClick)
	EVT_BUTTON			(efDrawPanel::ID_ButtonZoomReset,
	efDrawPanel::OnButtonZoomResetClick)
	EVT_BUTTON			(efDrawPanel::ID_ButtonResetOrigin,
	efDrawPanel::OnButtonResetOriginClick)
	EVT_BUTTON			(efDrawPanel::ID_ButtonHideButtons,
	efDrawPanel::OnButtonHideButtonsClick)
END_EVENT_TABLE()

efDrawPanel::efDrawPanel()
{
	m_Simulation = NULL;
	m_AreButtonsHidden = false;
	m_IsLeftDown = false;
	m_MovingObject = NULL;
	m_DrawAxes = false;
}

efDrawPanel::efDrawPanel(
	wxWindow *parent,
	wxWindowID id,
	const wxPoint &pos,
	const wxSize &size,
	long style,
	const wxString &name
)
{
	efDrawPanel::Create(parent, id, pos, size, style, name);
}

bool efDrawPanel::Create(
	wxWindow *parent,
	wxWindowID id,
	const wxPoint &pos,
	const wxSize &size,
	long style,
	const wxString &name
)
{
	if (!	wxPanel::Create(parent, id, pos, size,
			style | wxFULL_REPAINT_ON_RESIZE, name))
		return false;

	SetBackgroundStyle(wxBG_STYLE_CUSTOM);

	m_Simulation = NULL;
	m_AreButtonsHidden = false;
	m_IsLeftDown = false;
	m_MovingObject = NULL;
	m_DrawAxes = false;

	m_ButtonZoomIn =
	new wxButton(
	this, ID_ButtonZoomIn, _("Z+"), wxPoint(5, 5), wxSize(36, 36));

	m_ButtonZoomOut =
	new wxButton(
	this, ID_ButtonZoomOut, _("Z-"), wxPoint(46, 5), wxSize(36, 36));

	m_ButtonZoomReset =
	new wxButton(
	this, ID_ButtonZoomReset, _("Z0"), wxPoint(87, 5), wxSize(36, 36));

	m_ButtonResetOrigin =
	new wxButton(
	this, ID_ButtonResetOrigin, _("O"), wxPoint(5, 46), wxSize(36, 36));

	m_ButtonHideButtons =
	new wxButton(
	this, ID_ButtonHideButtons, _("H"), wxPoint(46, 46), wxSize(36, 36));
}

efDrawPanel::~efDrawPanel()
{

}

void efDrawPanel::DrawArrow(wxDC &dc, double x1, double y1, double x2, double y2)
{
	double alpha = atan2(y2 - y1, x2 - x1);
	double beta = M_PI / 6;	// 30 deg
	double l = 20;
	dc.DrawLine(x1, y1, x2, y2);
	dc.DrawLine(x2, y2, x2 - l * cos(alpha - beta), y2 - l * sin(alpha - beta));
	dc.DrawLine(x2, y2, x2 - l * cos(alpha + beta), y2 - l * sin(alpha + beta));
}

void efDrawPanel::CalculateGrid()
{
	// Canberk Sönmez
	m_GridLinesX.clear();
	m_GridLinesY.clear();
	int wi, he;
	GetSize(&wi ,&he);
	double omegax, omegay, lx, ly, vx, vy, dx, dy;
	dx = m_Simulation->Environment->OX-wi/2;
	dy = m_Simulation->Environment->OY-he/2;
	lx = m_Simulation->Environment->SX*m_Simulation->Environment->SpacePerMetre;
	ly = m_Simulation->Environment->SY*m_Simulation->Environment->SpacePerMetre;
	omegax = fmod(dx,lx);
	omegay = fmod(dy,ly);
	vx = -(dx-omegax)/lx;
	vy = -(dy-omegay)/ly;
	for (double px=wi+omegax; px>=0;px-=lx, vx-=1)
		m_GridLinesX.push_back(grid_line(vx, px));
	for (double py=-omegay; py<= he; py+=ly, vy-=1)
		m_GridLinesY.push_back(grid_line(vy, py));
}

void efDrawPanel::DrawGrid(wxDC &dc)
{

#if 0

	int wi, he;
	double ox, oy;
#if 0
	int a = 0, b = 0;	// just wanna be sure
#endif
	GetSize(&wi ,&he);
	ox = m_Simulation->Environment->OX;
	oy = m_Simulation->Environment->OY;

	// draw vert - lines
	double lx = m_Simulation->Environment->SpacePerMetre * m_Simulation->Environment->SX;
	double omegax = fmod((-ox-wi/2), lx);
	for (double x = -omegax; x <= wi; x += lx)
	{
#if 0
		a++;
#endif
		dc.DrawLine(x, 0, x, he);
	}

	// draw horz - lines
	double ly = m_Simulation->Environment->SpacePerMetre * m_Simulation->Environment->SY;
	double omegay = fmod((oy-he/2), ly);
	for (double y = -omegay; y <= he; y += ly)
	{
#if 0
		b++;
#endif
		dc.DrawLine(0, y, wi, y);
	}

#if 0
	dc.DrawText(wxString::Format(wxT("DrawGrid info :\na=%d\nb=%d"), a, b), 200, 5);
#endif

	DrawArrow(dc, wi / 2 + m_Simulation->Environment->OX, he, wi / 2 + m_Simulation->Environment->OX, 0);
	DrawArrow(dc, 0, he / 2 - m_Simulation->Environment->OY, wi, he / 2 - m_Simulation->Environment->OY);

#endif	// My new code gonna be tried

	int wi, he;
	GetSize(&wi ,&he);

	for (vector<grid_line>::const_iterator it = m_GridLinesX.begin();
		it != m_GridLinesX.end();
		it++)
	{
		dc.DrawLine((*it).pos, 0, (*it).pos, he);
	}

	for (vector<grid_line>::const_iterator it = m_GridLinesY.begin();
		it != m_GridLinesY.end();
		it++)
	{
		dc.DrawLine(0, (*it).pos, wi, (*it).pos);
	}

	DrawArrow(dc, wi / 2 + m_Simulation->Environment->OX, he, wi / 2 + m_Simulation->Environment->OX, 0);
	DrawArrow(dc, 0, he / 2 - m_Simulation->Environment->OY, wi, he / 2 - m_Simulation->Environment->OY);
}

void efDrawPanel::DrawAxisNumbers(wxDC &dc)
{
	int wi, he;
	GetSize(&wi ,&he);
	for (vector<grid_line>::const_iterator it = m_GridLinesX.begin();
		it != m_GridLinesX.end();
		it++)
	{
		if ((*it).value != 0)
			dc.DrawText(wxString::Format(wxT("%.3f"), (*it).value), (*it).pos + 5, he/2 - m_Simulation->Environment->OY + 10);
	}

	for (vector<grid_line>::const_iterator it = m_GridLinesY.begin();
		it != m_GridLinesY.end();
		it++)
	{
		if ((*it).value != 0)
			dc.DrawText(wxString::Format(wxT("%.3f"), (*it).value), wi/2 + m_Simulation->Environment->OX + 10, (*it).pos);
	}
}

void efDrawPanel::DrawObject(wxDC &dc, const efObject *obj)
{
	dc.SetBrush(wxBrush(obj->FillColour));
	dc.SetPen(wxPen(obj->StrokeColour));
	dc.DrawCircle(TransformPoint(m_Simulation->TransformPoint(obj->X, obj->Y)), obj->Radius);
	wxPoint A, B;
	if (obj->ElectricForce.Show)
	{
		dc.SetPen(wxPen(obj->ElectricForce.Colour));
		A = m_Simulation->TransformPoint(obj->X, obj->Y);
		B = m_Simulation->TransformPoint(
			obj->X + m_Simulation->CalculateElectricForceX(obj),
			obj->Y + m_Simulation->CalculateElectricForceY(obj));
		A = TransformPoint(A);
		B = TransformPoint(B);
		DrawArrow(dc, A.x, A.y, B.x, B.y);
	}
	if (obj->ElectricForceX.Show)
	{
		dc.SetPen(wxPen(obj->ElectricForceX.Colour));
		A = m_Simulation->TransformPoint(obj->X, obj->Y);
		B = m_Simulation->TransformPoint(
			obj->X + m_Simulation->CalculateElectricForceX(obj),
			obj->Y);
		A = TransformPoint(A);
		B = TransformPoint(B);
		DrawArrow(dc, A.x, A.y, B.x, B.y);
	}
	if (obj->ElectricForceY.Show)
	{
		dc.SetPen(wxPen(obj->ElectricForceY.Colour));
		A = m_Simulation->TransformPoint(obj->X, obj->Y);
		B = m_Simulation->TransformPoint(
			obj->X,
			obj->Y + m_Simulation->CalculateElectricForceY(obj));
		A = TransformPoint(A);
		B = TransformPoint(B);
		DrawArrow(dc, A.x, A.y, B.x, B.y);
	}
}

void efDrawPanel::DrawInformation(wxDC &dc)
{
	// PERHAPS NEAR FUTURE ? BUT NOT NOW !
}

wxPoint efDrawPanel::TransformPoint(const wxPoint &p)
{
	int wi, he;
	GetSize(&wi, &he);
	return wxPoint(wi / 2 + p.x, he / 2 - p.y);
}

efObject *efDrawPanel::FindObjectFromPoint(const wxPoint &p, size_t &index)
{
	index = m_Simulation->Objects.size();
	wxPoint A;
	for (vector<efObject *>::const_reverse_iterator it = m_Simulation->Objects.rbegin();
		it != m_Simulation->Objects.rend();
		it++)
	{
		index--;
		A = m_Simulation->TransformPoint((*it)->X, (*it)->Y);
		A = TransformPoint(A);
		if ((A.x - p.x) * (A.x - p.x) + (A.y - p.y) * (A.y - p.y) <= ((*it)->Radius) * ((*it)->Radius))
		{
			return *it;
		}
	}
	return NULL;
}

void efDrawPanel::OnPaint(wxPaintEvent &event)
{
	wxBufferedPaintDC dc(this);
	if (m_Simulation == NULL)
	{
		dc.SetBackground(*wxWHITE);
		dc.Clear();
		return;
	}
	else
	{
		if (m_Simulation->Environment == NULL)
		{
			dc.SetBackground(*wxWHITE);
			dc.Clear();
			return;
		}
	}

	dc.SetBackground(m_Simulation->Environment->BackgroundColour);
	dc.Clear();

	CalculateGrid();
	if (m_Simulation->Environment->ShowGrid)
	{
		dc.SetPen(wxPen(m_Simulation->Environment->GridColour));
		DrawGrid(dc);
	}

	if (m_DrawAxes)
	{
		dc.SetPen(wxPen(m_Simulation->Environment->GridColour));
		DrawAxisNumbers(dc);
	}

	for (vector<efObject *>::const_iterator it = m_Simulation->Objects.begin();
		it !=  m_Simulation->Objects.end();
		it++)
	{
		DrawObject(dc, *it);
	}

}

void efDrawPanel::OnMouseLeftDown(wxMouseEvent &event)
{
	m_PointA = event.GetPosition();
	m_MovingObject = FindObjectFromPoint(m_PointA, m_MovingObjectIndex);
	if (m_MovingObject != NULL)
	{
		m_ObjectListViewer->DoListCtrlSelectObject(m_MovingObjectIndex);
	}
	m_IsLeftDown = true;
}

void efDrawPanel::OnMouseLeftUp(wxMouseEvent &event)
{
	m_IsLeftDown = false;
	m_MovingObject = NULL;
}

void efDrawPanel::OnMouseLeftDclick(wxMouseEvent &event)
{
	event.Skip();
}

void efDrawPanel::OnMouseRightDown(wxMouseEvent &event)
{
	event.Skip();
}

void efDrawPanel::OnMouseRightUp(wxMouseEvent &event)
{
	event.Skip();
}

void efDrawPanel::OnMouseRightDclick(wxMouseEvent &event)
{
	event.Skip();
}

void efDrawPanel::OnMouseMiddleDown(wxMouseEvent &event)
{
	event.Skip();
}

void efDrawPanel::OnMouseMiddleUp(wxMouseEvent &event)
{
	event.Skip();
}

void efDrawPanel::OnMouseMiddleDclick(wxMouseEvent &event)
{
	event.Skip();
}

#define ABS(x)	(((x)<0)?-(x):(x))

void efDrawPanel::OnMouseMotion(wxMouseEvent &event)
{
	if (m_IsLeftDown)
	{
		if (m_MovingObject != NULL)
		{
			bool neargridx = false, neargridy = false;
			double gridxval, gridyval;
			if (m_Simulation->Environment->SnapToGrid)
			{
				for (vector<grid_line>::const_iterator it = m_GridLinesX.begin();
					it != m_GridLinesX.end();
					it++)
				{
					if (ABS((*it).pos-event.GetPosition().x)<=5)
					{
						neargridx = true;
						gridxval = (*it).value;
					}
				}

				for (vector<grid_line>::const_iterator it = m_GridLinesY.begin();
					it != m_GridLinesY.end();
					it++)
				{
					if (ABS((*it).pos-event.GetPosition().y)<=5)
					{
						neargridy = true;
						gridyval = (*it).value;
					}
				}
			}

			if (!(neargridx || neargridy))
			{
				m_MovingObject->X += (event.GetPosition().x - m_PointA.x) / (m_Simulation->Environment->SX * m_Simulation->Environment->SpacePerMetre);
				m_MovingObject->Y -= (event.GetPosition().y - m_PointA.y) / (m_Simulation->Environment->SY * m_Simulation->Environment->SpacePerMetre);
				if (m_ObjectListViewer != NULL)
				{
					m_ObjectListViewer->DoListCtrlEditObject(m_MovingObjectIndex, m_MovingObject);
					m_ObjectListViewer->DoListCtrlRefreshItems();
					m_ObjectListViewer->DoRefreshSystemElectricPotential();
				}
			}
			else
			{
				if (neargridx) m_MovingObject->X = gridxval;
				if (neargridy) m_MovingObject->Y = gridyval;
				if (m_ObjectListViewer != NULL)
				{
					m_ObjectListViewer->DoListCtrlEditObject(m_MovingObjectIndex, m_MovingObject);
					m_ObjectListViewer->DoListCtrlRefreshItems();
					m_ObjectListViewer->DoRefreshSystemElectricPotential();
				}
				Refresh(true);
				return;
			}
		}
		else
		{
			m_Simulation->Environment->OX += event.GetPosition().x - m_PointA.x;
			m_Simulation->Environment->OY -= event.GetPosition().y - m_PointA.y;
		}
		m_PointA = event.GetPosition();
		Refresh(true);
	}
}

void efDrawPanel::OnMouseWheel(wxMouseEvent &event)
{
	// Write A better code, focusing the event.GetPosition()
	int lines = event.GetWheelRotation() / event.GetWheelDelta();
	if (lines < 0)
	{
		for (int i = 0; i < -lines; i++)
			ZoomOut();
		return;
	}
	for (int i = 0; i < lines; i++)
		ZoomIn();
}

void efDrawPanel::OnKeyDown(wxKeyEvent &event)
{
	event.Skip();
}

void efDrawPanel::OnKeyUp(wxKeyEvent &event)
{
	event.Skip();
}

void efDrawPanel::OnEraseBackground(wxEraseEvent &event)
{

}

void efDrawPanel::OnButtonZoomInClick(wxCommandEvent &event)
{
	ZoomIn();
}

void efDrawPanel::OnButtonZoomOutClick(wxCommandEvent &event)
{
	ZoomOut();
}

void efDrawPanel::OnButtonZoomResetClick(wxCommandEvent &event)
{
	ZoomReset();
}

void efDrawPanel::OnButtonResetOriginClick(wxCommandEvent &event)
{
	ResetOrigin();
}

void efDrawPanel::OnButtonHideButtonsClick(wxCommandEvent &event)
{
	if (m_AreButtonsHidden)
	{
		m_ButtonZoomIn->Show(true);
		m_ButtonZoomOut->Show(true);
		m_ButtonZoomReset->Show(true);
		m_ButtonResetOrigin->Show(true);
		m_ButtonHideButtons->SetPosition(wxPoint(46, 46));
		m_ButtonHideButtons->SetLabel(wxT("H"));
		m_AreButtonsHidden = false;
	}
	else
	{
		m_ButtonZoomIn->Show(false);
		m_ButtonZoomOut->Show(false);
		m_ButtonZoomReset->Show(false);
		m_ButtonResetOrigin->Show(false);
		m_ButtonHideButtons->SetPosition(wxPoint(5, 5));
		m_ButtonHideButtons->SetLabel(wxT("S"));
		m_AreButtonsHidden = true;
	}
}

void efDrawPanel::SetSimulation(efSimulation *sim, bool refresh)
{
	m_Simulation = sim;
	if (refresh)
		Refresh(true);
}

efSimulation *efDrawPanel::GetSimulation() const
{
	return m_Simulation;
}

void efDrawPanel::SetObjectListViewer(efObjectListViewer *view)
{
	m_ObjectListViewer = view;
}

efObjectListViewer *efDrawPanel::GetObjectListViewer() const
{
	return m_ObjectListViewer;
}

void efDrawPanel::ZoomIn()
{
	m_Simulation->Environment->SX *= 2;
	m_Simulation->Environment->SY *= 2;
	Refresh(true);
}

void efDrawPanel::ZoomOut()
{
	m_Simulation->Environment->SX /= 2;
	m_Simulation->Environment->SY /= 2;
	Refresh(true);
}

void efDrawPanel::ZoomReset()
{
	m_Simulation->Environment->SX = 1;
	m_Simulation->Environment->SY = 1;
	Refresh(true);
}

void efDrawPanel::ResetOrigin()
{
	m_Simulation->Environment->OX = 0;
	m_Simulation->Environment->OY = 0;
	Refresh(true);
}

