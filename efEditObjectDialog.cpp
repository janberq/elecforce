#include "efEditObjectDialog.h"

efEditObjectDialog::efEditObjectDialog( wxWindow* parent, efSimulation *sim, efObject *obj )
:
ELECFORCE_GUI_EditObjectDialog( parent )
{
	m_Simulation = sim;
	m_ObjectToEdit = obj;
	TransferDataToWindow();
}

bool efEditObjectDialog::TransferDataFromWindow()
{
	if (m_ObjectToEdit == NULL)
		return false;

	m_ObjectToEdit->Name = m_textCtrlName->GetValue();
	m_textCtrlX->GetValue().ToDouble(&(m_ObjectToEdit->X));
	m_textCtrlY->GetValue().ToDouble(&(m_ObjectToEdit->Y));
	m_textCtrlRadius->GetValue().ToDouble(&(m_ObjectToEdit->Radius));
	m_textCtrlElectricCharge->GetValue().ToDouble(&(m_ObjectToEdit->ElectricCharge));
	m_ObjectToEdit->ElectricForce.Show = m_checkBoxShowElectricForce->GetValue();
	m_ObjectToEdit->ElectricForceX.Show = m_checkBoxShowElectricForceX->GetValue();
	m_ObjectToEdit->ElectricForceY.Show = m_checkBoxShowElectricForceY->GetValue();
	m_ObjectToEdit->FillColour = m_colourPickerFillColour->GetColour();
	m_ObjectToEdit->StrokeColour = m_colourPickerStrokeColour->GetColour();
	m_ObjectToEdit->ElectricForce.Colour = m_colourPickerElectricForce->GetColour();
	m_ObjectToEdit->ElectricForceX.Colour = m_colourPickerElectricForceX->GetColour();
	m_ObjectToEdit->ElectricForceY.Colour = m_colourPickerElectricForceY->GetColour();

	return true;
}

bool efEditObjectDialog::TransferDataToWindow()
{
	if (m_Simulation == NULL || m_ObjectToEdit == NULL)
		return false;

	m_textCtrlName->SetValue(m_ObjectToEdit->Name);
	m_textCtrlX->SetValue(wxString::Format(wxT("%f"), m_ObjectToEdit->X));
	m_textCtrlY->SetValue(wxString::Format(wxT("%f"), m_ObjectToEdit->Y));
	m_textCtrlRadius->SetValue(wxString::Format(wxT("%f"), m_ObjectToEdit->Radius));
	m_textCtrlElectricCharge->SetValue(wxString::Format(wxT("%f"), m_ObjectToEdit->ElectricCharge));
	m_checkBoxShowElectricForce->SetValue(m_ObjectToEdit->ElectricForce.Show);
	m_checkBoxShowElectricForceX->SetValue(m_ObjectToEdit->ElectricForceX.Show);
	m_checkBoxShowElectricForceY->SetValue(m_ObjectToEdit->ElectricForceY.Show);
	m_colourPickerElectricForce->SetColour(m_ObjectToEdit->ElectricForce.Colour);
	m_colourPickerElectricForceX->SetColour(m_ObjectToEdit->ElectricForceX.Colour);
	m_colourPickerElectricForceY->SetColour(m_ObjectToEdit->ElectricForceY.Colour);
	m_colourPickerFillColour->SetColour(m_ObjectToEdit->FillColour);
	m_colourPickerStrokeColour->SetColour(m_ObjectToEdit->StrokeColour);

	// other calc stuff

	double electricForce, electricForceX, electricForceY, electricForceTheta;
	double electricPotential;

	electricForceX = m_Simulation->CalculateElectricForceX(m_ObjectToEdit);
	electricForceY = m_Simulation->CalculateElectricForceY(m_ObjectToEdit);
	electricForce = sqrt(electricForceX * electricForceX + electricForceY * electricForceY);
	electricForceTheta = atan2(electricForceY, electricForceX);
	electricPotential = m_Simulation->CalculateElectricPotential(m_ObjectToEdit);

	m_textCtrlElectricForce->SetValue(wxString::Format(wxT("%f"), electricForce));
	m_textCtrlElectricForceX->SetValue(wxString::Format(wxT("%f"), electricForceX));
	m_textCtrlElectricForceY->SetValue(wxString::Format(wxT("%f"), electricForceY));
	m_textCtrlElectricForceTheta->SetValue(wxString::Format(wxT("%f"), electricForceTheta * 180 / M_PI));
	m_textCtrlElectricPotential->SetValue(wxString::Format(wxT("%f"), electricPotential));

	return true;
}

void efEditObjectDialog::OnButtonApplyClick( wxCommandEvent& event )
{
	if (Validate() && TransferDataFromWindow())
	{
		if (IsModal())
		{
			EndModal(wxID_OK);
		}
		else
		{
			SetReturnCode(wxID_OK);
			this->Show(false);
		}
	}
	else
	{
		if (IsModal())
		{
			EndModal(wxID_CANCEL);
		}
		else
		{
			SetReturnCode(wxID_CANCEL);
			this->Show(false);
		}
	}
}

void efEditObjectDialog::OnButtonCancelClick( wxCommandEvent& event )
{
	if (IsModal())
	{
		EndModal(wxID_CANCEL);
	}
	else
	{
		SetReturnCode(wxID_CANCEL);
		this->Show(false);
	}
}

void efEditObjectDialog::OnButtonResetClick( wxCommandEvent& event )
{
	TransferDataToWindow();
}

void efEditObjectDialog::OnButtonDefaultsClick( wxCommandEvent& event )
{
	efObject *oldobj = m_ObjectToEdit;
	m_ObjectToEdit = new efObject;
	TransferDataToWindow();
	delete m_ObjectToEdit;
	m_ObjectToEdit = oldobj;
}
